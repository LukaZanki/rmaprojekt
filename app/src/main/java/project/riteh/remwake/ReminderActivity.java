package project.riteh.remwake;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import project.riteh.remwake.adapters.ReminderAdapter;
import project.riteh.remwake.objectclasses.Reminder;
import project.riteh.remwake.sql.DataSource;

public class ReminderActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{


    private DataSource datasource;
    private List<Reminder> reminderList= new ArrayList<>();
    private RecyclerView recyclerView;
    private ReminderAdapter mAdapter;
    private FloatingActionButton fab;
    protected GoogleApiClient mGoogleApiClient=null;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);
        recyclerView=(RecyclerView)findViewById(R.id.rv);

        fab=(FloatingActionButton)findViewById(R.id.fab);
        datasource = new DataSource(getApplicationContext());

        //conect to googleplay services
        connectToApi();

        prepareData();
        initializeAdapter();
        recyclerViewSetup();




    }
    @Override
    protected void onResume() {
        super.onResume();
        prepareData();
        initializeAdapter();
        if(!mGoogleApiClient.isConnected()){
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        Log.v("connection", "Connection disconect.");
        super.onStop();
    }

    private void prepareData() {
    datasource.open();
    reminderList = datasource.getAllReminders();
    datasource.close();


}
    public void initializeAdapter(){
        mAdapter= new ReminderAdapter(ReminderActivity.this, reminderList, datasource, mGoogleApiClient);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.v("connection", "Connected.");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v("connection", "Connection suspended.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.v("connection", "Connection failed.");
    }

private void connectToApi(){
    mGoogleApiClient = new GoogleApiClient.Builder(this)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build();
    mGoogleApiClient.connect();
}
 private void recyclerViewSetup(){
     RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
     recyclerView.setLayoutManager(mLayoutManager);
     recyclerView.setItemAnimator(new DefaultItemAnimator());

     fab.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             Intent i = new Intent(v.getContext(),CreateReminderActivity.class);
             startActivity(i);
         }
     });
 }


}

