package project.riteh.remwake;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import java.util.Calendar;

import project.riteh.remwake.objectclasses.Reminder;
import project.riteh.remwake.reminderservice.ReminderReceiver;
import project.riteh.remwake.sql.DataSource;

public class ReminderEditActivity extends AppCompatActivity {

    private EditText title;
    private EditText text;
    private String id;
    private DataSource datasource;
    private Reminder reminder;
    private TextView alarm_location;
    private int PLACE_PICKER_REQUEST=1;
    private LatLng latLng=null;
    private double latitude;
    private double longitude;
    int type = 0;
    private ReminderReceiver reminderReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_edit);
        title=(EditText)findViewById(R.id.title);
        text=(EditText)findViewById(R.id.text);
        alarm_location=(TextView)findViewById(R.id.alarm_location);
        datasource = new DataSource(getApplicationContext());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras= getIntent().getExtras();
        if(extras != null)
             id = extras.getString("id_reminder");

        datasource.open();
        reminder=datasource.getReminderWithId((Long.parseLong(id)));
        datasource.close();
        title.setText(reminder.getTitle());
        text.setText(reminder.getText());
        //iz baze
        alarm_location.setText(reminder.getAddress());
        latitude=reminder.getLatitude();
        longitude=reminder.getLongitude();
        alarm_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alarm_location.setText("");
                longitude=0;
                latitude=0;
                type=0;
                reminder.setAllOptions("", ""+type);
                datasource.open();
                datasource.updateReminder(reminder);
                datasource.close();
            }
        });



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_update_reminder, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_accept) {
            reminder.setTitle(title.getText().toString());
            reminder.setText(text.getText().toString());
            saveReminder();
            finish();
            return true;
        }else if (id == android.R.id.home){
            finish();
            return true;
        }
        else if ((id==R.id.action_delete)){
            deleteReminder();
            finish();
            return true;
        }
        else if(id==R.id.action_alarm) {
            //set dialog
            setDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void saveReminder() {
        datasource.open();
        datasource.updateReminder(reminder);
        datasource.close();
    }

    private void deleteReminder(){
        if (Boolean.parseBoolean(reminder.getIs_toggled())) {
            if (reminder.getType().equals("2")) {
                reminderReceiver = new ReminderReceiver();
                reminderReceiver.CancelReminder(getApplicationContext(), Integer.parseInt(id));
                datasource.open();
                datasource.deleteReminder((Long.parseLong(id)));
                datasource.close();
                Toast.makeText(ReminderEditActivity.this, "Reminder deleted!", Toast.LENGTH_SHORT).show();
            }else if (reminder.getType().equals("1")){
                    Toast.makeText(ReminderEditActivity.this, "Location is still active. Disable location first.", Toast.LENGTH_SHORT).show();
            }else if (reminder.getType().equals("0")) {
                datasource.open();
                datasource.deleteReminder((Long.parseLong(id)));
                datasource.close();
                Toast.makeText(ReminderEditActivity.this, "Reminder deleted!", Toast.LENGTH_SHORT).show();
            }
        }else{
            datasource.open();
            datasource.deleteReminder((Long.parseLong(id)));
            datasource.close();
            Toast.makeText(ReminderEditActivity.this, "Reminder deleted!", Toast.LENGTH_SHORT).show();
        }
    }

    private void setPlacePicker(){

        try {
            PlacePicker.IntentBuilder intentBuilder =
                    new PlacePicker.IntentBuilder();

            Intent intent = intentBuilder.build(ReminderEditActivity.this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }


    private void setDialog(){

        final CharSequence[] items = {
                "Pick date and time", "Pick a place"
        };
        AlertDialog.Builder builder =  new AlertDialog.Builder(this)
                .setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        switch(which){
                            case 0:
                                //time and date picker
                                setTime();
                                break;
                            case 1:
                                //location picker
                                setPlacePicker();
                                break;
                        }
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void setTime() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                setDate(selectedHour, selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private void setDate(final int selectedHour, final int selectedMinute) {
        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog;
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear++;
                alarm_location.setText("Time: "+ formatTime(selectedHour) + ":" + formatTime(selectedMinute)+" Date: "+formatTime(dayOfMonth)+"."+formatTime(monthOfYear)+"."+year+".");
                reminder.setTimeDate(selectedHour, selectedMinute, year,monthOfYear,dayOfMonth, alarm_location.getText().toString());
                reminder.setType("2");
                type = 2;
            }
        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setTitle("Select Date");
        datePickerDialog.show();
    }

    public String formatTime(int time){
        String timeString;
        if (time<10){
            timeString = "0"+time;
        }else{
            timeString = ""+time;
        }
        return timeString;
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {

        if (requestCode == PLACE_PICKER_REQUEST
                && resultCode == Activity.RESULT_OK) {

            final Place place = PlacePicker.getPlace(this, data);

            if(place==null)
                return;
            //dohvacanje adrese i longitude
            latLng=place.getLatLng();
            longitude=latLng.longitude;
            latitude=latLng.latitude;
            CharSequence location = place.getAddress();
            Toast.makeText(this,"Lat:"+latLng.latitude + " Long"+latLng.longitude+ "Address:"+location.toString() , Toast.LENGTH_SHORT).show();

            alarm_location.setText(location.toString());

            if(location.toString().equals(""))
                alarm_location.setText(""+latitude+","+longitude);

            reminder.setType("1");
            reminder.setPlace(longitude, latitude, alarm_location.getText().toString());
            type=1;

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
