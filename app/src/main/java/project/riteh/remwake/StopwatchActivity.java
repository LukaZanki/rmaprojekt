package project.riteh.remwake;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import project.riteh.remwake.objectclasses.Stopwatch;

public class StopwatchActivity extends AppCompatActivity {

    private Context ct;

    private ImageButton startButton;
    private ImageButton pauseButton;
    private ImageButton stopButton;
    private ImageButton splitButton;

    private TextView timerValue;
    LinearLayout splitLayout;

    //Using stopwatch class
    Stopwatch sw = new Stopwatch();
    final int REFRESH_RATE = 1;
    final int MSG_START_TIMER = 0;
    final int MSG_STOP_TIMER = 1;
    final int MSG_UPDATE_TIMER = 2;
    final int MSG_RESUME_TIMER = 3;
    final int MSG_PAUSE_TIMER = 4;
    int toggleButtons = 0;
    boolean isRunning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stopwatch);
        ct = getApplicationContext();

        //Set button listeners
        setButtonListeners();
        //Set text views
        setTextView();
    }

    private void setButtonListeners() {
        //Button init
        startButton = (ImageButton) findViewById(R.id.startButton);
        pauseButton = (ImageButton) findViewById(R.id.pauseButton);
        stopButton =(ImageButton) findViewById(R.id.stopButton);
        splitButton = (ImageButton) findViewById(R.id.splitButton);

        //Layout init
        splitLayout = (LinearLayout) findViewById(R.id.splitLayout);
        //Start button
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isRunning){
                    isRunning = true;
                    mHandler.sendEmptyMessage(MSG_START_TIMER);
                }else{
                    mHandler.sendEmptyMessage(MSG_RESUME_TIMER);
                }
                //TOGGLE BUTTONS
                toggleButtons = 0;
                handleButtons();
            }
        });

        //Pause button
        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHandler.sendEmptyMessage(MSG_PAUSE_TIMER);
                //TOGGLE BUTTONS
                toggleButtons = 1;
                handleButtons();
            }
        });

        //Stop button
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHandler.sendEmptyMessage(MSG_STOP_TIMER);
                //TOGGLE BUTTONS
                toggleButtons = 1;
                handleButtons();
                timerValue.setText(R.string.timerVal);
                splitLayout.removeAllViews();
                isRunning = false;
            }
        });
        splitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView lblSplit = new TextView(ct);
                lblSplit.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f));
                lblSplit.setGravity(View.TEXT_ALIGNMENT_CENTER);
                lblSplit.setTextSize(35);
                lblSplit.setTextColor(Color.WHITE);
                lblSplit.setText(sw.toStringStopwatchSplit());
                splitLayout.addView(lblSplit, 0);
            }
        });
    }

    private void setTextView() {
        timerValue = (TextView) findViewById(R.id.timerValue);
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_START_TIMER:
                    sw.start(); //start timer
                    mHandler.sendEmptyMessage(MSG_UPDATE_TIMER);
                    break;
                case MSG_UPDATE_TIMER:
                    timerValue.setText(sw.toStringStopwatch());
                    mHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIMER, REFRESH_RATE); //text view is updated every millisecond,
                    break;                                  //though the timer is still running
                case MSG_STOP_TIMER:
                    sw.stop();//stop timer
                    mHandler.removeMessages(MSG_UPDATE_TIMER); // no more updates.
                    break;
                case MSG_RESUME_TIMER:
                    sw.resume();
                    mHandler.sendEmptyMessage(MSG_UPDATE_TIMER);
                    break;
                case MSG_PAUSE_TIMER:
                    sw.pause();
                    mHandler.removeMessages(MSG_UPDATE_TIMER);
                    break;
                default:
                    break;
            }
        }
    };
    public void handleButtons(){
        //PAUSE AND SPLIT VISIBLE
        if (toggleButtons == 0) {
            pauseButton.setVisibility(View.VISIBLE);
            startButton.setVisibility(View.INVISIBLE);
            splitButton.setVisibility(View.VISIBLE);
            stopButton.setVisibility(View.INVISIBLE);
        }//START AND STOP VISIBLE
        else if (toggleButtons == 1){
            startButton.setVisibility(View.VISIBLE);
            pauseButton.setVisibility(View.INVISIBLE);
            stopButton.setVisibility(View.VISIBLE);
            splitButton.setVisibility(View.INVISIBLE);
        }
    }
}
