package project.riteh.remwake;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.TextView;
import at.grabner.circleprogress.CircleProgressView;

public class TimerEditDialog extends DialogFragment {


    NumberPicker npMinute;
    NumberPicker npSeconds;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        builder.setView(inflater.inflate(R.layout.dialog_timer, null));


        builder.setMessage("Set time")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Dialog f = (Dialog) dialog;
                        TextView minutes=(TextView) getActivity().findViewById(R.id.tvMinute);
                        TextView seconds= (TextView) getActivity().findViewById(R.id.tvSecond);
                        npMinute=(NumberPicker)getActivity().findViewById(R.id.minutes);
                        npSeconds=(NumberPicker)getActivity().findViewById(R.id.seconds);


                        NumberPicker npSeconds= (NumberPicker) f.findViewById(R.id.seconds);
                        NumberPicker npMinutes= (NumberPicker) f.findViewById(R.id.minutes);

                        CircleProgressView circleView=(CircleProgressView)getActivity().findViewById(R.id.circleView);

                        minutes.setText(String.format("%02d",npMinutes.getValue()));
                        seconds.setText(String.format("%02d",npSeconds.getValue()));
                        circleView.setValue(npSeconds.getValue());

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        // Create the AlertDialog object and return it

        return builder.create();


    }

}
