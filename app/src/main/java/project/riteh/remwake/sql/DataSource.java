package project.riteh.remwake.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import project.riteh.remwake.objectclasses.Alarm;
import project.riteh.remwake.objectclasses.AlarmWithMusic;
import project.riteh.remwake.objectclasses.Music;
import project.riteh.remwake.objectclasses.Reminder;

//Data handling - input and output
public class DataSource {

    //DATABASE OBJECTS
    private static SQLiteDatabase database;
    private static SQLiteHelper dbHelper;

    public DataSource(Context context) {
        dbHelper = new SQLiteHelper(context);
    }

    //OPEN CONNECTION TO DATABASE
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    //CLOSE CONNECTION TO DATABASE
    public void close() {
        dbHelper.close();
    }

    //STRING ARRAY CONTAINING ALL COLUMNS FROM ALARM
    private String[] allColumns_alarm = new String[]{
            SQLiteHelper.COLUMN_ID_ALARM,
            SQLiteHelper.COLUMN_HOURS,
            SQLiteHelper.COLUMN_MINUTES,
            SQLiteHelper.COLUMN_IS_TOGGLED,
            SQLiteHelper.COLUMN_IS_MONDAY,
            SQLiteHelper.COLUMN_IS_TUESDAY,
            SQLiteHelper.COLUMN_IS_WEDNESDAY,
            SQLiteHelper.COLUMN_IS_THURSDAY,
            SQLiteHelper.COLUMN_IS_FRIDAY,
            SQLiteHelper.COLUMN_IS_SATURDAY,
            SQLiteHelper.COLUMN_IS_SUNDAY,
            SQLiteHelper.COLUMN_YEAR,
            SQLiteHelper.COLUMN_MONTH,
            SQLiteHelper.COLUMN_DAY_OF_MONTH,
            SQLiteHelper.COLUMN_DAY_OR_DATE,
            SQLiteHelper.COLUMN_IS_SMART_RISE,
            SQLiteHelper.COLUMN_VIBRATION,
            SQLiteHelper.COLUMN_CHALLENGE};

    //STRING ARRAY CONTAINING ALL COLUMNS FROM REMINDER
    private String[] allColumns_reminder = new String[]{
            SQLiteHelper.COLUMN_ID_REMINDER,
            SQLiteHelper.COLUMN_TITLE_REMINDER,
            SQLiteHelper.COLUMN_TEXT_REMINDER,
            SQLiteHelper.COLUMN_LONGITUDE,
            SQLiteHelper.COLUMN_LATITUDE,
            SQLiteHelper.COLUMN_ADDRESS,
            SQLiteHelper.COLUMN_TYPE,
            SQLiteHelper.COLUMN_HOURS,
            SQLiteHelper.COLUMN_MINUTES,
            SQLiteHelper.COLUMN_IS_TOGGLED,
            SQLiteHelper.COLUMN_YEAR,
            SQLiteHelper.COLUMN_MONTH,
            SQLiteHelper.COLUMN_DAY_OF_MONTH};

    //STRING ARRAY CONTAINING ALL COLUMNS FROM ALARM MUSIC
    private String[] allColumns_music = new String[]{
            SQLiteHelper.COLUMN_ID_ALARM_MUSIC,
            SQLiteHelper.COLUMN_NAME,
            SQLiteHelper.COLUMN_MUSIC_PATH };

    //STRING ARRAY CONTAINING ALL COLUMNS FROM ALARM WITH MUSIC
    private String[] allColumns_alarm_music = new String[]{
            SQLiteHelper.COLUMN_ID_ALARM,
            SQLiteHelper.COLUMN_ID_ALARM_MUSIC };

    //INSERT VALUES INTO TABLE ALARM
    public long createAlarm(String hours, String minutes, String isToggled, String isMon, String isTue, String isWed, String isThu, String isFri, String isSat, String isSun,String year,String month,String day_of_month,String which, String isSmartRise, String vibration, String challenge) {

        ContentValues values = new ContentValues();
        values.put(SQLiteHelper.COLUMN_HOURS, hours);
        values.put(SQLiteHelper.COLUMN_MINUTES, minutes);
        values.put(SQLiteHelper.COLUMN_IS_TOGGLED, isToggled);
        values.put(SQLiteHelper.COLUMN_IS_MONDAY, isMon);
        values.put(SQLiteHelper.COLUMN_IS_TUESDAY, isTue);
        values.put(SQLiteHelper.COLUMN_IS_WEDNESDAY, isWed);
        values.put(SQLiteHelper.COLUMN_IS_THURSDAY, isThu);
        values.put(SQLiteHelper.COLUMN_IS_FRIDAY, isFri);
        values.put(SQLiteHelper.COLUMN_IS_SATURDAY, isSat);
        values.put(SQLiteHelper.COLUMN_IS_SUNDAY, isSun);
        values.put(SQLiteHelper.COLUMN_YEAR, year);
        values.put(SQLiteHelper.COLUMN_MONTH, month);
        values.put(SQLiteHelper.COLUMN_DAY_OF_MONTH, day_of_month);
        values.put(SQLiteHelper.COLUMN_DAY_OR_DATE, which);
        values.put(SQLiteHelper.COLUMN_IS_SMART_RISE, isSmartRise);
        values.put(SQLiteHelper.COLUMN_VIBRATION, vibration);
        values.put(SQLiteHelper.COLUMN_CHALLENGE, challenge);

        long insertId = database.insert(SQLiteHelper.TABLE_NAME_ALARM, null,
                values);

        return insertId;
    }

    //SELECT ALL ALARMS
    public ArrayList<Alarm> getAllAlarms() {
        ArrayList<Alarm> alarms = new ArrayList<Alarm>();

        Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_ALARM,
                allColumns_alarm, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Alarm alarm = cursorToAlarm(cursor);
                cursor.moveToNext();
                alarms.add(alarm);
            }
            cursor.close();
        }
        return alarms;
    }

    //SELECT ALARM WITH ID
    public Alarm getAlarmWithId(long id) {
        Alarm alarm = new Alarm();

        Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_ALARM,
                allColumns_alarm, SQLiteHelper.COLUMN_ID_ALARM + " = " + id, null, null, null, null);
        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                alarm = cursorToAlarm(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return alarm;
    }

    public void deleteAlarm(long id_alarm) {
        database.delete(SQLiteHelper.TABLE_NAME_ALARM, SQLiteHelper.COLUMN_ID_ALARM
                + " = " + id_alarm, null);
        deleteAlarmWithMusic(id_alarm);
    }
    public void deleteAlarmWithMusic(long id_alarm) {
        database.delete(SQLiteHelper.TABLE_NAME_ALARM_WITH_MUSIC, SQLiteHelper.COLUMN_ID_ALARM
                + " = " + id_alarm, null);
    }

    public void updateToggleAlarm(long id_alarm, String toggle){
        String sql = "UPDATE "+SQLiteHelper.TABLE_NAME_ALARM+" SET "+SQLiteHelper.COLUMN_IS_TOGGLED+"='"+toggle+"' WHERE "+SQLiteHelper.COLUMN_ID_ALARM+"="+id_alarm;
        database.execSQL(sql);
    }

    public long updateAllAlarm(Alarm alarm){
        ContentValues values = new ContentValues();
        values.put(SQLiteHelper.COLUMN_HOURS, alarm.getHours());
        values.put(SQLiteHelper.COLUMN_MINUTES, alarm.getMinutes());
        values.put(SQLiteHelper.COLUMN_IS_TOGGLED, alarm.getIs_toggled());
        values.put(SQLiteHelper.COLUMN_IS_MONDAY, alarm.getIsMon());
        values.put(SQLiteHelper.COLUMN_IS_TUESDAY, alarm.getIsTue());
        values.put(SQLiteHelper.COLUMN_IS_WEDNESDAY, alarm.getIsWed());
        values.put(SQLiteHelper.COLUMN_IS_THURSDAY, alarm.getIsThu());
        values.put(SQLiteHelper.COLUMN_IS_FRIDAY, alarm.getIsFri());
        values.put(SQLiteHelper.COLUMN_IS_SATURDAY, alarm.getIsSat());
        values.put(SQLiteHelper.COLUMN_IS_SUNDAY, alarm.getIsSun());
        values.put(SQLiteHelper.COLUMN_YEAR, alarm.getYear());
        values.put(SQLiteHelper.COLUMN_MONTH, alarm.getMonth());
        values.put(SQLiteHelper.COLUMN_DAY_OF_MONTH, alarm.getDay());
        values.put(SQLiteHelper.COLUMN_DAY_OR_DATE, alarm.getIsDayOrDate());
        values.put(SQLiteHelper.COLUMN_IS_SMART_RISE, alarm.getSmartRise());
        values.put(SQLiteHelper.COLUMN_VIBRATION, alarm.getVibration());
        values.put(SQLiteHelper.COLUMN_CHALLENGE, alarm.getChallenge());

        long id_alarm = database.update(SQLiteHelper.TABLE_NAME_ALARM, values,""+SQLiteHelper.COLUMN_ID_ALARM+ "="+alarm.getIDAlarm(), null);
        return id_alarm;
    }

    public long updateAlarmWithMusic(int id_alarm, int id_music){
        ContentValues values = new ContentValues();
        values.put(SQLiteHelper.COLUMN_ID_ALARM_MUSIC, id_music);

        long insertId = database.update(SQLiteHelper.TABLE_NAME_ALARM_WITH_MUSIC, values,""+SQLiteHelper.COLUMN_ID_ALARM+ "="+id_alarm, null);
        return insertId;
    }

    //SET OBJECT CLASS TO MATCH THE DATA
    private Alarm cursorToAlarm(Cursor cursor) {
        Alarm alarm = new Alarm();
        alarm.setIDAlarm(cursor.getString(0));
        alarm.setHours(cursor.getString(1));
        alarm.setMinutes(cursor.getString(2));
        alarm.setisToggled(cursor.getString(3));
        alarm.setIsMon(cursor.getString(4));
        alarm.setIsTue(cursor.getString(5));
        alarm.setIsWed(cursor.getString(6));
        alarm.setIsThu(cursor.getString(7));
        alarm.setIsFri(cursor.getString(8));
        alarm.setIsSat(cursor.getString(9));
        alarm.setIsSun(cursor.getString(10));
        alarm.setYear(cursor.getString(11));
        alarm.setMonth(cursor.getString(12));
        alarm.setDay(cursor.getString(13));
        alarm.setIsDayOrDate(cursor.getString(14));
        alarm.setSmartRise(cursor.getString(15));
        alarm.setVibration(cursor.getString(16));
        alarm.setChallenge(cursor.getString(17));
        return alarm;
    }

    //INSERT VALUES INTO TABLE REMINDER
    public void createReminder(Reminder reminder) {

        ContentValues values = new ContentValues();
        values.put(SQLiteHelper.COLUMN_TITLE_REMINDER, reminder.getTitle());
        values.put(SQLiteHelper.COLUMN_TEXT_REMINDER, reminder.getText());
        values.put(SQLiteHelper.COLUMN_LONGITUDE, reminder.getLongitude());
        values.put(SQLiteHelper.COLUMN_LATITUDE, reminder.getLatitude());
        values.put(SQLiteHelper.COLUMN_ADDRESS, reminder.getAddress());
        values.put(SQLiteHelper.COLUMN_TYPE, reminder.getType());
        values.put(SQLiteHelper.COLUMN_HOURS, reminder.getHours());
        values.put(SQLiteHelper.COLUMN_MINUTES, reminder.getMinutes());
        values.put(SQLiteHelper.COLUMN_IS_TOGGLED, reminder.getIs_toggled());
        values.put(SQLiteHelper.COLUMN_YEAR, reminder.getYear());
        values.put(SQLiteHelper.COLUMN_MONTH, reminder.getMonth());
        values.put(SQLiteHelper.COLUMN_DAY_OF_MONTH, reminder.getDay_of_month());

        long insertId = database.insert(SQLiteHelper.TABLE_NAME_REMINDER, null,
                values);
    }

    //SELECT ALL REMINDERS
    public ArrayList<Reminder> getAllReminders() {
        ArrayList<Reminder> reminders = new ArrayList<Reminder>();

        Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_REMINDER,
                allColumns_reminder, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Reminder reminder = cursorToReminder(cursor);
                cursor.moveToNext();
                reminders.add(reminder);
            }
            cursor.close();
        }
        return reminders;
    }
    //SET OBJECT CLASS REMINDER TO MATCH THE DATA
    private Reminder cursorToReminder(Cursor cursor) {
        Reminder reminder = new Reminder();
        reminder.setId(cursor.getString(0));
        reminder.setTitle(cursor.getString(1));
        reminder.setText(cursor.getString(2));
        reminder.setLongitude(cursor.getDouble(3));
        reminder.setLatitude(cursor.getDouble(4));
        reminder.setAddress(cursor.getString(5));
        reminder.setType(cursor.getString(6));
        reminder.setHours(cursor.getString(7));
        reminder.setMinutes(cursor.getString(8));
        reminder.setIs_toggled(cursor.getString(9));
        reminder.setYear(cursor.getString(10));
        reminder.setMonth(cursor.getString(11));
        reminder.setDay_of_month(cursor.getString(12));

        return reminder;
    }

    //SELECT REMINDER WITH ID
    public Reminder getReminderWithId(long id) {
        Reminder reminder = new Reminder();

        Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_REMINDER,
                allColumns_reminder, SQLiteHelper.COLUMN_ID_REMINDER + " = " + id, null, null, null, null);
        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                reminder = cursorToReminder(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return reminder;
    }
    public void deleteReminder(long id_reminder) {
        database.delete(SQLiteHelper.TABLE_NAME_REMINDER, SQLiteHelper.COLUMN_ID_REMINDER
                + " = " + id_reminder, null);
    }

    public long updateReminder(Reminder reminder){
        ContentValues values = new ContentValues();
        values.put(SQLiteHelper.COLUMN_TITLE_REMINDER, reminder.getTitle());
        values.put(SQLiteHelper.COLUMN_TEXT_REMINDER, reminder.getText());
        values.put(SQLiteHelper.COLUMN_LONGITUDE, reminder.getLongitude());
        values.put(SQLiteHelper.COLUMN_LATITUDE, reminder.getLatitude());
        values.put(SQLiteHelper.COLUMN_ADDRESS, reminder.getAddress());
        values.put(SQLiteHelper.COLUMN_TYPE, reminder.getType());
        values.put(SQLiteHelper.COLUMN_HOURS, reminder.getHours());
        values.put(SQLiteHelper.COLUMN_MINUTES, reminder.getMinutes());
        values.put(SQLiteHelper.COLUMN_IS_TOGGLED, reminder.getIs_toggled());
        values.put(SQLiteHelper.COLUMN_YEAR, reminder.getYear());
        values.put(SQLiteHelper.COLUMN_MONTH, reminder.getMonth());
        values.put(SQLiteHelper.COLUMN_DAY_OF_MONTH, reminder.getDay_of_month());

        long id_reminder = database.update(SQLiteHelper.TABLE_NAME_REMINDER, values,""+SQLiteHelper.COLUMN_ID_REMINDER+ "="+reminder.getId(), null);
        return id_reminder;
    }

    public ArrayList<Music> getAllMusic(){
        ArrayList<Music> almusic = new ArrayList<>();

        Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_ALARM_MUSIC,
                allColumns_music, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Music music = cursorToMusic(cursor);
                cursor.moveToNext();
                almusic.add(music);
            }
            cursor.close();
        }
        return almusic;
    }

    private Music cursorToMusic(Cursor cursor) {
        Music music = new Music();
        music.setId_music(cursor.getString(0));
        music.setMusic_name(cursor.getString(1));
        music.setMusic_path(cursor.getString(2));
        return music;
    }

    //SELECT MUSIC WITH ID
    public Music getMusicWithID(int id_music) {
        Music music = new Music();
        Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_ALARM_MUSIC,
                allColumns_music, SQLiteHelper.COLUMN_ID_ALARM_MUSIC + " = " + id_music, null, null, null, null);
        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                music = cursorToMusic(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return music;
    }

    //SELECT MUSIC WITH NAME
    public ArrayList<Music> getMusicWithName(ArrayList selectedItems) {
        ArrayList<Music> alMusic = new ArrayList<>();
        Music music;
        for(int i=0;i<selectedItems.size();i++) {
            Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_ALARM_MUSIC,
                    allColumns_music, SQLiteHelper.COLUMN_NAME + " = '" + selectedItems.get(i)+"'", null, null, null, null);
            if (cursor.moveToFirst()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    music = cursorToMusic(cursor);
                    alMusic.add(music);
                    cursor.moveToNext();
                }
                cursor.close();
            }
        }
        return alMusic;
    }

    //INSERT VALUES INTO TABLE ALARM WITH MUSIC (WHICH MUSIC WILL FIRE ON ALARM TRIGGER)
    public void createAlarmWithMusic(int id_alarm, int id_music) {

        ContentValues values = new ContentValues();
        values.put(SQLiteHelper.COLUMN_ID_ALARM, id_alarm);
        values.put(SQLiteHelper.COLUMN_ID_ALARM_MUSIC, id_music);

        long insertId = database.insert(SQLiteHelper.TABLE_NAME_ALARM_WITH_MUSIC, null,
                values);
    }

    //SELECT ALL MUSIC WITH ALARM ID
    public ArrayList<AlarmWithMusic> getAllMusicWithAlarmID(int id_alarm) {
        ArrayList<AlarmWithMusic> alAlarmWithMusic = new ArrayList<>();
        Alarm alarm;
        Music music;
        AlarmWithMusic alarmWithMusic;
        alarm = getAlarmWithId(id_alarm);
        Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_ALARM_WITH_MUSIC,
                allColumns_alarm_music, SQLiteHelper.COLUMN_ID_ALARM + " = " + id_alarm, null, null, null, null);
        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                music = getMusicWithID(Integer.parseInt(cursor.getString(1)));
                alarmWithMusic = alarmAndMusicToAlarmWithMusic(alarm,music);
                alAlarmWithMusic.add(alarmWithMusic);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return alAlarmWithMusic;
    }

    private AlarmWithMusic alarmAndMusicToAlarmWithMusic(Alarm alarm, Music music) {
        AlarmWithMusic alarmWithMusic = new AlarmWithMusic();
        alarmWithMusic.setAlarm(alarm);
        alarmWithMusic.setMusic(music);
        return alarmWithMusic;
    }

    public void updateQuickEdit(String hours, String minutes, long id_alarm, String isToggle) {
        String sql = "UPDATE "+SQLiteHelper.TABLE_NAME_ALARM+" SET "+SQLiteHelper.COLUMN_IS_TOGGLED+"='"+isToggle+"', "+SQLiteHelper.COLUMN_HOURS+"='"+hours+"', "+SQLiteHelper.COLUMN_MINUTES+"='"+minutes+ "' WHERE "+SQLiteHelper.COLUMN_ID_ALARM+"="+id_alarm;
        database.execSQL(sql);
    }

    public void updateToggleReminder(long id_reminder, String toggle) {
        String sql = "UPDATE "+SQLiteHelper.TABLE_NAME_REMINDER+" SET "+SQLiteHelper.COLUMN_IS_TOGGLED+"='"+toggle+"' WHERE "+SQLiteHelper.COLUMN_ID_REMINDER+"="+id_reminder;
        database.execSQL(sql);
    }
}
