package project.riteh.remwake;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setListeners();
    }

    public void setListeners(){
        RelativeLayout rlMenuAlarm = (RelativeLayout) findViewById(R.id.menubuttonalarm);
        rlMenuAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), AlarmActivity.class);
                startActivity(i);
            }
        });
        RelativeLayout rlMenuStopwatch = (RelativeLayout) findViewById(R.id.menubuttonstopwatch);
        rlMenuStopwatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), StopwatchActivity.class);
                startActivity(i);
            }
        });
        RelativeLayout rlMenuReminder = (RelativeLayout) findViewById(R.id.menubuttonreminder);
        rlMenuReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ReminderActivity.class);
                startActivity(i);
            }
        });
        RelativeLayout rlMenuCounter = (RelativeLayout) findViewById(R.id.menubuttoncounter);
        rlMenuCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), CounterActivity.class);
                startActivity(i);
            }
        });

    }
}
