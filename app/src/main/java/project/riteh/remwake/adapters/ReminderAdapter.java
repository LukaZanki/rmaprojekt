package project.riteh.remwake.adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.api.GoogleApiClient;
import java.util.Calendar;
import java.util.List;
import project.riteh.remwake.R;
import project.riteh.remwake.ReminderActivity;
import project.riteh.remwake.ReminderEditActivity;
import project.riteh.remwake.objectclasses.Reminder;
import project.riteh.remwake.reminderservice.ReminderReceiver;
import project.riteh.remwake.sql.DataSource;

public class ReminderAdapter extends RecyclerView.Adapter<ReminderAdapter.ReminderViewHolder>{

    private List<Reminder> reminderList;
    private Context mContext;
    private DataSource dataSource;
    private ReminderReceiver reminderReceiver;
    protected GoogleApiClient mGoogleApiClient=null;
    private Boolean permission = true;

    public class ReminderViewHolder extends RecyclerView.ViewHolder {
        public TextView title, text, location_alarm;
        public Switch switch_button;
        public CardView cardView;

        public ReminderViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            text = (TextView) view.findViewById(R.id.text);
            location_alarm=(TextView)view.findViewById(R.id.location_alarm);
            switch_button=(Switch)view.findViewById(R.id.switch_button);
            cardView = (CardView)view.findViewById(R.id.card_view_rem);
        }
    }

    public ReminderAdapter(Context mContext, List<Reminder> reminderList, DataSource dataSource, GoogleApiClient mGoogleApiClient){
        this.mContext = mContext;
        this.reminderList=reminderList;
        this.dataSource=dataSource;
        this.reminderReceiver = new ReminderReceiver();
        this.mGoogleApiClient = mGoogleApiClient;
    }


    @Override
    public ReminderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reminder_card, parent, false);

        return new ReminderViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ReminderViewHolder holder, final int position) {
        final Reminder reminder= reminderList.get(position);
        holder.title.setText(reminder.getTitle());
        holder.text.setText(reminder.getText());
        holder.location_alarm.setText(reminder.getAddress());
        holder.switch_button.setChecked(Boolean.parseBoolean(reminderList.get(position).getIs_toggled()));

        holder.switch_button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Reminder reminder = reminderList.get(position);
                if(isChecked) {
                    //SET TIME
                    if (reminder.getType().equals("2")) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(reminder.getHours()));
                        calendar.set(Calendar.MINUTE, Integer.parseInt(reminder.getMinutes()));
                        calendar.set(Calendar.SECOND, 00);
                        calendar.set(Calendar.YEAR, Integer.parseInt(reminder.getYear()));
                        calendar.set(Calendar.MONTH, Integer.parseInt(reminder.getMonth())-1);
                        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(reminder.getDay_of_month()));
                        if (System.currentTimeMillis() > calendar.getTimeInMillis()) {
                            Toast.makeText(mContext, "You set the reminders time in the past! Try again.", Toast.LENGTH_SHORT).show();
                            buttonView.setChecked(false);
                            dataSource.open();
                            dataSource.updateToggleReminder(Long.parseLong(reminder.getId()), "false");
                            dataSource.close();
                        }else{
                            dataSource.open();
                            dataSource.updateToggleReminder(Long.parseLong(reminder.getId()), "true");
                            dataSource.close();
                            reminderReceiver.SetReminder(mContext, calendar, Integer.parseInt(reminder.getId()),reminder.getTitle(),reminder.getText());
                            Toast.makeText(mContext, "Reminder set on: "+calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE)+" "+calendar.get(Calendar.DAY_OF_MONTH)+"."+(calendar.get(Calendar.MONTH)+1)+"."+calendar.get(Calendar.YEAR)+".", Toast.LENGTH_SHORT).show();
                        }

                    }else if (reminder.getType().equals("1")){
                        if (!mGoogleApiClient.isConnected()){
                            mGoogleApiClient.connect();
                        }
                        if (mGoogleApiClient.isConnected()) {
                            ActivityCompat.requestPermissions((ReminderActivity) mContext,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    1);
                            if (permission) {
                                reminderReceiver.setGeofencingParameteres(reminder,mGoogleApiClient,mContext);
                                dataSource.open();
                                dataSource.updateToggleReminder(Long.parseLong(reminder.getId()), "true");
                                dataSource.close();
                                reminderReceiver.setGeofence();
                                reminderReceiver.startGeofence();
                                Toast.makeText(mContext, "Geofence added", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(mContext, "Permissions denied", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(mContext, "Not connected to Google map API", Toast.LENGTH_SHORT).show();
                            buttonView.setChecked(false);
                        }
                    }
                }  else{
                    dataSource.open();
                    dataSource.updateToggleReminder(Long.parseLong(reminder.getId()),"false");
                    dataSource.close();
                    //DEACTIVATE ALARM
                    if (reminder.getType().equals("2")) {
                        reminderReceiver.CancelReminder(mContext, Integer.parseInt(reminder.getId()));
                    }else if (reminder.getType().equals("1")) {
                        reminderReceiver.setGeofencingParameteres(reminder,mGoogleApiClient,mContext);
                        reminderReceiver.removeGeofences();
                        Toast.makeText(mContext, "Geofence removed", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        //IF NOT DATE TIME HIDE TOGGLE
        if (reminder.getType().equals("0")){
            holder.switch_button.setVisibility(View.INVISIBLE);
            reminderReceiver.CancelReminder(mContext, Integer.parseInt(reminder.getId()));
            reminderReceiver.setGeofencingParameteres(reminder, mGoogleApiClient,mContext);
            reminderReceiver.removeGeofences();
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Reminder reminder = reminderList.get(position);
                reminderReceiver.setGeofencingParameteres(reminder, mGoogleApiClient,mContext);
                Intent intentEditReminder = new Intent(mContext,ReminderEditActivity.class);
                intentEditReminder.putExtra("id_reminder", reminderList.get(position).getId());
                String transitionName = mContext.getResources().getString(R.string.transition_alarm_cover);
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation((Activity)mContext, v, transitionName);
                mContext.startActivity(intentEditReminder, options.toBundle());
            }
        });
    }
    //GET PERIMISSION BY USER
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    this.permission = true;
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    this.permission = false;
                }
                return;
            }
        }
    }

    @Override
    public int getItemCount() {
        return reminderList.size();
    }
}
