package project.riteh.remwake.adapters;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import java.util.Calendar;
import java.util.List;
import project.riteh.remwake.EditAlarmActivity;
import project.riteh.remwake.R;
import project.riteh.remwake.alarmservice.AlarmReceiver;
import project.riteh.remwake.objectclasses.Alarm;
import project.riteh.remwake.sql.DataSource;

public class AlarmAdapter extends RecyclerView.Adapter<AlarmAdapter.AlarmViewHolder>{

    private List<Alarm> alarmList;
    private Context mContext;
    DataSource datasource;
    private AlarmReceiver alarmreceiver;

    public class AlarmViewHolder extends RecyclerView.ViewHolder {
        public TextView tvHours, tvMinutes, tvMonday, tvTuesday, tvWednesday,tvThursday,tvFriday,tvSaturday,tvSunday,tvDay,tvMonth,tvYear;
        public Switch toggleAlarm;
        public CardView cardView;
        LinearLayout showDate;
        RelativeLayout timeChoose;

        public AlarmViewHolder(View view) {
            super(view);
            cardView = (CardView)itemView.findViewById(R.id.card_view);
            tvHours = (TextView) itemView.findViewById(R.id.hours);
            tvMinutes = (TextView) itemView.findViewById(R.id.minutes);
            tvMonday = (TextView) itemView.findViewById(R.id.monday);
            tvTuesday = (TextView) itemView.findViewById(R.id.tuesday);
            tvWednesday = (TextView) itemView.findViewById(R.id.wednesday);
            tvThursday = (TextView) itemView.findViewById(R.id.thursday);
            tvFriday = (TextView) itemView.findViewById(R.id.friday);
            tvSaturday = (TextView) itemView.findViewById(R.id.saturday);
            tvSunday = (TextView) itemView.findViewById(R.id.sunday);
            tvDay = (TextView) itemView.findViewById(R.id.day);
            tvMonth = (TextView) itemView.findViewById(R.id.month);
            tvYear = (TextView) itemView.findViewById(R.id.year);
            toggleAlarm = (Switch) itemView.findViewById(R.id.toggle_alarm);
            showDate = (LinearLayout)itemView.findViewById(R.id.showDate);
            timeChoose = (RelativeLayout)itemView.findViewById(R.id.timeChoose);

        }

    }

    public AlarmAdapter(Context mContext, List<Alarm> alarmList, DataSource datasource){
        this.mContext = mContext;
        this.alarmList=alarmList;
        this.datasource = datasource;
        alarmreceiver = new AlarmReceiver();
    }

    @Override
    public AlarmViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.alarm_card, parent, false);

        return new AlarmViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AlarmViewHolder holder, final int position) {
        holder.tvHours.setText(alarmList.get(position).getFormatedHours());
        holder.tvMinutes.setText(alarmList.get(position).getFormatedMinutes());
        holder.toggleAlarm.setChecked(Boolean.parseBoolean(alarmList.get(position).getisToggled()));

        setupTextViews(holder, position);
        holder.toggleAlarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                Alarm alarm = alarmList.get(position);
                if(isChecked){
                    datasource.open();
                    datasource.updateToggleAlarm(Long.parseLong(alarm.getIDAlarm()),"true");
                    datasource.close();
                    //SET TIME
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(alarm.getHours()));
                    calendar.set(Calendar.MINUTE, Integer.parseInt(alarm.getMinutes()));
                    calendar.set(Calendar.SECOND, 00);
                    if (alarm.isDayOrDate.equals("2")){
                        calendar.set(Calendar.YEAR, Integer.parseInt(alarm.getYear()));
                        calendar.set(Calendar.MONTH, Integer.parseInt(alarm.getMonth()));
                        calendar.set(Calendar.DAY_OF_WEEK, Integer.parseInt(alarm.getDay()));
                    }
                    if (System.currentTimeMillis() > calendar.getTimeInMillis()) {
                        calendar.add(Calendar.DATE, 1);
                    }
                    //SETUP ON DAY  //IF DAYS ARE NOT TOGGLED OR DATE IS SET DO:
                    if ((!Boolean.parseBoolean(alarm.getIsMon()) && !Boolean.parseBoolean(alarm.getIsTue()) && !Boolean.parseBoolean(alarm.getIsWed()) && !Boolean.parseBoolean(alarm.getIsThu()) && !Boolean.parseBoolean(alarm.getIsFri()) && !Boolean.parseBoolean(alarm.getIsSat()) && !Boolean.parseBoolean(alarm.getIsSun()))||alarm.getIsDayOrDate().equals("2")){
                        //SET ALARM BASIC
                        alarmreceiver.SetAlarm(mContext,calendar, Integer.parseInt(alarm.getIDAlarm()));
                        //IF SMART RISE SET ANOTHER ALARM 30 MINUTES EARLIER
                        if (Boolean.parseBoolean(alarm.getSmartRise())){
                            //SMART RISE CALENDAR
                            Calendar smartCalendar = calendar;
                            smartCalendar.setTimeInMillis(calendar.getTimeInMillis()-1800000);
                            alarmreceiver.SetAlarm(mContext, smartCalendar, Integer.parseInt(alarm.getIDAlarm()));
                        }
                    }else {
                        //CHECK WHICH DAYS TO TRIGGER
                        if (Boolean.parseBoolean(alarm.getIsMon())){
                            calendar.set(Calendar.DAY_OF_WEEK, 2); // 2 is int for monday
                            //SET ALARM WITH DAY OF WEEK
                            alarmreceiver.SetAlarm(mContext, calendar, Integer.parseInt(alarm.getIDAlarm()));
                            if (Boolean.parseBoolean(alarm.getSmartRise())){
                                //SMART RISE CALENDAR
                                Calendar smartCalendar = calendar;
                                smartCalendar.setTimeInMillis(calendar.getTimeInMillis()-1800000);
                                alarmreceiver.SetAlarm(mContext, smartCalendar, Integer.parseInt(alarm.getIDAlarm()));
                            }
                        }
                        if (Boolean.parseBoolean(alarm.getIsTue())){
                            calendar.set(Calendar.DAY_OF_WEEK, 3); // 3 is int for tuesday
                            //SET ALARM WITH DAY OF WEEK
                            alarmreceiver.SetAlarm(mContext, calendar, Integer.parseInt(alarm.getIDAlarm()));
                            if (Boolean.parseBoolean(alarm.getSmartRise())){
                                //SMART RISE CALENDAR
                                Calendar smartCalendar = calendar;
                                smartCalendar.setTimeInMillis(calendar.getTimeInMillis()-1800000);
                                alarmreceiver.SetAlarm(mContext, smartCalendar, Integer.parseInt(alarm.getIDAlarm()));
                            }
                        }
                        if (Boolean.parseBoolean(alarm.getIsWed())){
                            calendar.set(Calendar.DAY_OF_WEEK, 4); // 4 is int for wednesday
                            //SET ALARM WITH DAY OF WEEK
                            alarmreceiver.SetAlarm(mContext, calendar, Integer.parseInt(alarm.getIDAlarm()));
                            if (Boolean.parseBoolean(alarm.getSmartRise())){
                                //SMART RISE CALENDAR
                                Calendar smartCalendar = calendar;
                                smartCalendar.setTimeInMillis(calendar.getTimeInMillis()-1800000);
                                alarmreceiver.SetAlarm(mContext, smartCalendar, Integer.parseInt(alarm.getIDAlarm()));
                            }
                        }
                        if (Boolean.parseBoolean(alarm.getIsThu())){
                            calendar.set(Calendar.DAY_OF_WEEK, 5); // 5 is int for thursday
                            //SET ALARM WITH DAY OF WEEK
                            alarmreceiver.SetAlarm(mContext, calendar, Integer.parseInt(alarm.getIDAlarm()));
                            if (Boolean.parseBoolean(alarm.getSmartRise())){
                                //SMART RISE CALENDAR
                                Calendar smartCalendar = calendar;
                                smartCalendar.setTimeInMillis(calendar.getTimeInMillis()-1800000);
                                alarmreceiver.SetAlarm(mContext, smartCalendar, Integer.parseInt(alarm.getIDAlarm()));
                            }
                        }
                        if (Boolean.parseBoolean(alarm.getIsFri())){
                            calendar.set(Calendar.DAY_OF_WEEK, 6); // 6 is int for friday
                            //SET ALARM WITH DAY OF WEEK
                            alarmreceiver.SetAlarm(mContext, calendar, Integer.parseInt(alarm.getIDAlarm()));
                            if (Boolean.parseBoolean(alarm.getSmartRise())){
                                //SMART RISE CALENDAR
                                Calendar smartCalendar = calendar;
                                smartCalendar.setTimeInMillis(calendar.getTimeInMillis()-1800000);
                                alarmreceiver.SetAlarm(mContext, smartCalendar, Integer.parseInt(alarm.getIDAlarm()));
                            }
                        }
                        if (Boolean.parseBoolean(alarm.getIsSat())){
                            calendar.set(Calendar.DAY_OF_WEEK, 7); // 7 is int for saturday
                            //SET ALARM WITH DAY OF WEEK
                            alarmreceiver.SetAlarm(mContext, calendar, Integer.parseInt(alarm.getIDAlarm()));
                            if (Boolean.parseBoolean(alarm.getSmartRise())){
                                //SMART RISE CALENDAR
                                Calendar smartCalendar = calendar;
                                smartCalendar.setTimeInMillis(calendar.getTimeInMillis()-1800000);
                                alarmreceiver.SetAlarm(mContext, smartCalendar, Integer.parseInt(alarm.getIDAlarm()));
                            }
                        }
                        if (Boolean.parseBoolean(alarm.getIsSun())){
                            calendar.set(Calendar.DAY_OF_WEEK, 1); // 1 is int for sunday
                            //SET ALARM WITH DAY OF WEEK
                            alarmreceiver.SetAlarm(mContext, calendar, Integer.parseInt(alarm.getIDAlarm()));
                            if (Boolean.parseBoolean(alarm.getSmartRise())){
                                //SMART RISE CALENDAR
                                Calendar smartCalendar = calendar;
                                smartCalendar.setTimeInMillis(calendar.getTimeInMillis()-1800000);
                                alarmreceiver.SetAlarm(mContext, smartCalendar, Integer.parseInt(alarm.getIDAlarm()));
                            }
                        }
                    }
                }   else{
                    datasource.open();
                    datasource.updateToggleAlarm(Long.parseLong(alarm.getIDAlarm()),"false");
                    datasource.close();
                    //DEACTIVATE ALARM
                    alarmreceiver.CancelAlarm(mContext, Integer.parseInt(alarm.getIDAlarm()));
                }

            }
        });
        holder.timeChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePicker = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        Alarm alarm = alarmList.get(position);
                        holder.tvHours.setText(""+selectedHour);
                        holder.tvMinutes.setText(""+selectedMinute);
                        datasource.open();
                        datasource.updateQuickEdit(""+selectedHour,""+selectedMinute,Long.parseLong(alarm.getIDAlarm()),"false");
                        alarmList = datasource.getAllAlarms();
                        datasource.close();
                        holder.toggleAlarm.setChecked(false);
                        notifyDataSetChanged();
                    }
                },Integer.parseInt(alarmList.get(position).getHours()) , Integer.parseInt(alarmList.get(position).getMinutes()), true);//true - 24 hour time
                timePicker.setTitle("Select Time");
                timePicker.show();
            }
        });

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentEditAlarm = new Intent(mContext,EditAlarmActivity.class);
                intentEditAlarm.putExtra("id_alarm", alarmList.get(position).getIDAlarm());
                String transitionName = mContext.getResources().getString(R.string.transition_alarm_cover);
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation((Activity)mContext, v, transitionName);
                mContext.startActivity(intentEditAlarm, options.toBundle());

            }
        });
    }

    private void setupTextViews(AlarmViewHolder holder, int position) {
        if (alarmList.get(position).isDayOrDate.equals("2")){
            holder.showDate.setVisibility(View.VISIBLE);
            holder.tvDay.setText(alarmList.get(position).getFormatedDay());
            holder.tvMonth.setText(alarmList.get(position).getFormatedMonth());
            holder.tvYear.setText(alarmList.get(position).getYear());
        }else{
            holder.showDate.setVisibility(View.INVISIBLE);
            if (Boolean.parseBoolean(alarmList.get(position).getIsMon())) {
                holder.tvMonday.setTextColor(Color.GREEN);
            } else {
                holder.tvMonday.setTextColor(mContext.getResources().getColor(R.color.secondary_text));
            }
            if (Boolean.parseBoolean(alarmList.get(position).getIsTue())) {
                holder.tvTuesday.setTextColor(Color.GREEN);
            } else {
                holder.tvTuesday.setTextColor(mContext.getResources().getColor(R.color.secondary_text));
            }
            if (Boolean.parseBoolean(alarmList.get(position).getIsWed())) {
                holder.tvWednesday.setTextColor(Color.GREEN);
            } else {
                holder.tvWednesday.setTextColor(mContext.getResources().getColor(R.color.secondary_text));
            }
            if (Boolean.parseBoolean(alarmList.get(position).getIsThu())) {
                holder.tvThursday.setTextColor(Color.GREEN);
            } else {
                holder.tvThursday.setTextColor(mContext.getResources().getColor(R.color.secondary_text));
            }
            if (Boolean.parseBoolean(alarmList.get(position).getIsFri())) {
                holder.tvFriday.setTextColor(Color.GREEN);
            } else {
                holder.tvFriday.setTextColor(mContext.getResources().getColor(R.color.secondary_text));
            }
            if (Boolean.parseBoolean(alarmList.get(position).getIsSat())) {
                holder.tvSaturday.setTextColor(Color.GREEN);
            } else {
                holder.tvSaturday.setTextColor(mContext.getResources().getColor(R.color.secondary_text));
            }
            if (Boolean.parseBoolean(alarmList.get(position).getIsSun())) {
                holder.tvSunday.setTextColor(Color.GREEN);
            } else {
                holder.tvSunday.setTextColor(mContext.getResources().getColor(R.color.secondary_text));
            }
        }

    }

    @Override
    public int getItemCount() {
        return alarmList.size();
    }
}


