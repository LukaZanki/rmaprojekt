package project.riteh.remwake.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import java.util.ArrayList;
import project.riteh.remwake.R;
import project.riteh.remwake.objectclasses.Alarm;
import project.riteh.remwake.objectclasses.AlarmWithMusic;
import project.riteh.remwake.objectclasses.Music;
import project.riteh.remwake.sql.DataSource;

public class OptionsFragment extends Fragment {

    CheckBox cbSmartRise;
    ImageButton imbSmartRiseHelp;
    RadioGroup rgVib;
    RadioButton rbNoVib;
    RadioButton rbMusicVib;
    RadioButton rbOnlyVib;
    RadioGroup rgCha;
    RadioButton rbNoCha;
    RadioButton rbShake;
    RadioButton rbMath;
    Button selectMusic;
    OnDataPassOptionsSmartRise dataPasserSmartRise;
    OnDataPassOptionsVibration dataPasserVibration;
    OnDataPassOptionsChallenge dataPasserChallenge;
    OnDataPassOptionsMusic dataPasserMusic;
    ArrayList selectedItems = new ArrayList();
    DataSource dataSource;
    String[] musicList = null;
    int idVb=1;
    int idCh=1;
    boolean[] defaultChecked;

    //FOR EDIT
    long id_alarm=0;
    Alarm alarm;

    public OptionsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity a) {
        super.onAttach(a);
        dataPasserSmartRise = (OnDataPassOptionsSmartRise) a;
        dataPasserVibration = (OnDataPassOptionsVibration) a;
        dataPasserChallenge = (OnDataPassOptionsChallenge) a;
        dataPasserMusic = (OnDataPassOptionsMusic) a;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_options, container, false);
        //INIT DATASOURCE
        dataSource = new DataSource(getActivity().getApplicationContext());
        //OPTIONS FOR SMART RISE
        cbSmartRise = (CheckBox) v.findViewById(R.id.smart_rise);
        imbSmartRiseHelp = (ImageButton) v.findViewById(R.id.smart_rise_help);
        //OPTIONS FOR VIBRATION
        rgVib = (RadioGroup) v.findViewById(R.id.radiogroup_vibration);
        rbNoVib = (RadioButton) v.findViewById(R.id.no_vibration);
        rbMusicVib = (RadioButton) v.findViewById(R.id.vibration_and_music);
        rbOnlyVib = (RadioButton) v.findViewById(R.id.only_vibration);
        //OPTIONS FOR CHALLENGE
        rgCha = (RadioGroup) v.findViewById(R.id.radiogroup_challenge);
        rbNoCha = (RadioButton) v.findViewById(R.id.no_challenge);
        rbShake = (RadioButton) v.findViewById(R.id.shake_challenge);
        rbMath = (RadioButton) v.findViewById(R.id.math_puzzle);
        //MUSIC BUTTON
        selectMusic = (Button) v.findViewById(R.id.select_music_button);
        //SET LISTENERS
        setListeners();
        //ADD DEFAULT ITEM TO SELECTED ITEM
        musicList = getMusicList();
        selectedItems.add(musicList[0]);
        defaultChecked = setDefaultChecked(musicList);

        if(!getArguments().isEmpty()) {
            id_alarm = Long.parseLong(getArguments().getString("id_alarm").toString());
            dataSource.open();
            alarm = dataSource.getAlarmWithId(id_alarm);
            dataSource.close();
            //CHALLENGE
            if (alarm.getChallenge().equals("1")){
                rgCha.check(R.id.no_challenge);
            }else if (alarm.getChallenge().equals("2")){
                rgCha.check(R.id.shake_challenge);
            }else if (alarm.getChallenge().equals("3")){
                rgCha.check(R.id.math_puzzle);
            }

            //VIBRATION
            if (alarm.getVibration().equals("1")){
                rgVib.check(R.id.no_vibration);
            }else if (alarm.getVibration().equals("2")){
                rgVib.check(R.id.vibration_and_music);
            }else if (alarm.getVibration().equals("3")){
                rgVib.check(R.id.only_vibration);
            }
            //SELECTED MUSIC
            defaultChecked = setSavedDefaultChecked(getMusicList());
            passDataOptionsMusic(selectedItems);
            //SMART RISE
            cbSmartRise.setChecked(Boolean.parseBoolean(alarm.getSmartRise()));
            passDataOptionsSmartRise(cbSmartRise.isChecked());
        }
        return v;
    }

    private void setListeners() {
        //SMART RISE TRUE FALSE
        cbSmartRise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cbSmartRise.isChecked()){
                    passDataOptionsSmartRise(cbSmartRise.isChecked());
                }else{
                    passDataOptionsSmartRise(cbSmartRise.isChecked());
                }
            }
        });
        //INFORMATION SMART RISE DIALOG
        imbSmartRiseHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle("Smart Rise");
                alertDialog.setMessage("Smart rise fires a low volume sound 30 minutes before the regular alarm to try to catch you on your sleep cycle and wake you up lightly");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        });
        //LISTENER FOR VIBRATION RADIO GROUP
        rgVib.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup rGroup, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)rGroup.findViewById(checkedId);
                //find the id of a button
                if(checkedRadioButton.getId()==R.id.no_vibration){
                    idVb = 1;
                }else if(checkedRadioButton.getId()==R.id.vibration_and_music){
                    idVb = 2;
                }else if(checkedRadioButton.getId()==R.id.only_vibration){
                    idVb = 3;
                }
                passDataOptionsVibration(idVb);
                Log.i("OPTIONS FRAGMENT", "ID OF VIBRATION: " + idVb);
            }
        });
        //LISTENER FOR CHALLENGE RADIO GROUP
        rgCha.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup rGroup, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)rGroup.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                //find the id of a button
                if(checkedRadioButton.getId()==R.id.no_challenge){
                    idCh = 1;
                }else if(checkedRadioButton.getId()==R.id.shake_challenge){
                    idCh = 2;
                }else if(checkedRadioButton.getId()==R.id.math_puzzle){
                    idCh = 3;
                }
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked) {
                    passDataOptionsChallenge(idCh);
                    Log.i("OPTIONS FRAGMENT", "ID OF CHALLENGE: "+idCh);
                }
            }
        });
        //LISTENER FOR MUSIC
        selectMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Select Music:")
                        .setMultiChoiceItems(musicList, defaultChecked,
                                new DialogInterface.OnMultiChoiceClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which,
                                                        boolean isChecked) {
                                        if (isChecked) {
                                            // If the user checked the item, add it to the selected items
                                            defaultChecked[which] = true;
                                            selectedItems.add(musicList[which]);
                                            for(int i=0;i<selectedItems.size();i++) {
                                                Log.i("OPTIONS FRAGMENT", "SelectedItem["+i+"]:" + selectedItems.get(i));
                                            }
                                        } else if (selectedItems.contains(musicList[which])) {
                                            // Else, if the item is already in the array, remove it
                                            defaultChecked[which] = false;
                                            selectedItems.remove(musicList[which]);
                                            for(int i=0;i<selectedItems.size();i++) {
                                                Log.i("OPTIONS FRAGMENT", "SelectedItem["+i+"]:" + selectedItems.get(i));
                                            }
                                        }
                                    }
                                })
                        .setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                passDataOptionsMusic(selectedItems);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                            }
                        });
                // Create the AlertDialog object
                builder.create();
                builder.show();
            }
        });
    }

    private boolean[] setSavedDefaultChecked(String[] musicList) {
        boolean[] bol = new boolean[musicList.length];
        ArrayList<AlarmWithMusic> almusic = new ArrayList<>();
        dataSource.open();
        almusic = dataSource.getAllMusicWithAlarmID((int) id_alarm);
        dataSource.close();
        for (int i = 0; i < almusic.size(); i++) {
            bol[Integer.parseInt(almusic.get(i).getMusic().getId_music())-1] = true;
            selectedItems.add(musicList[Integer.parseInt(almusic.get(i).getMusic().getId_music())-1]);
        }
        return bol;
    }

    private boolean[] setDefaultChecked(String[] musicList) {
        boolean[] bol = new boolean[musicList.length];
        bol[0] = true;
        return bol;
    }

    public String[] getMusicList(){
        ArrayList<Music> almusic = new ArrayList<>();
        String[] musicarray;
        dataSource.open();
        almusic = dataSource.getAllMusic();
        dataSource.close();
        musicarray = new String[almusic.size()];
        for (int i = 0; i<almusic.size();i++){
            musicarray[i] = almusic.get(i).getMusic_name();
        }

        return musicarray;
    }

    public void passDataOptionsSmartRise(boolean smartRise) {
        dataPasserSmartRise.onDataPassOptionsSmartRise(smartRise);
    }
    public void passDataOptionsVibration(int vibration) {
        dataPasserVibration.onDataPassOptionsVibration(vibration);
    }
    public void passDataOptionsChallenge(int challenge) {
        dataPasserChallenge.onDataPassOptionsChallenge(challenge);
    }
    public void passDataOptionsMusic(ArrayList selectedItems) {
        dataPasserMusic.onDataPassOptionsMusic(selectedItems);
    }

    public interface OnDataPassOptionsSmartRise {
        public void onDataPassOptionsSmartRise(boolean smartRise);
    }
    public interface OnDataPassOptionsVibration {
        public void onDataPassOptionsVibration(int vibration);
    }
    public interface OnDataPassOptionsChallenge {
        public void onDataPassOptionsChallenge(int challenge);
    }
    public interface OnDataPassOptionsMusic {
        public void onDataPassOptionsMusic(ArrayList selectedItems);
    }
}
