package project.riteh.remwake.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TimePicker;
import project.riteh.remwake.R;
import project.riteh.remwake.objectclasses.Alarm;
import project.riteh.remwake.sql.DataSource;

public class TimeFragment extends Fragment {

    private TimePicker tp;
    OnDataPass dataPasser;
    long id_alarm=0;
    DataSource datasource;
    Alarm alarm;

    public TimeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity a) {
        super.onAttach(a);
        dataPasser = (OnDataPass) a;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_time, container, false);
        alarm = new Alarm();
        datasource = new DataSource(getContext());
        if(!getArguments().isEmpty()) {
            id_alarm = Long.parseLong(getArguments().getString("id_alarm").toString());
            datasource.open();
            alarm = datasource.getAlarmWithId(id_alarm);
            datasource.close();
        }
        tp = (TimePicker) v.findViewById(R.id.timePicker);
        tp.setIs24HourView(true);
        tp.animate();
        passData(""+tp.getCurrentHour(),""+tp.getCurrentMinute());
        tp.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                passData(""+tp.getCurrentHour(),""+tp.getCurrentMinute());
            }
        });
        if(!getArguments().isEmpty()){
            tp.setCurrentHour(Integer.parseInt(alarm.getHours()));
            tp.setCurrentMinute(Integer.parseInt(alarm.getMinutes()));
        }

        return v;
    }

    public void passData(String hours, String minutes) {
        dataPasser.onDataPass(hours, minutes);
    }

    public interface OnDataPass {
        public void onDataPass(String hours, String minutes);
    }
}


