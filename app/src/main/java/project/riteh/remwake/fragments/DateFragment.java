package project.riteh.remwake.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ToggleButton;
import java.util.ArrayList;
import java.util.Calendar;
import project.riteh.remwake.R;
import project.riteh.remwake.objectclasses.Alarm;
import project.riteh.remwake.sql.DataSource;

public class DateFragment extends Fragment {

    OnDataPassDay dataPasserDay;
    OnDataPassDate dataPasserDate;
    OnDataPassWhich dataPasserWhich;
    private ToggleButton tbMon;
    private ToggleButton tbTue;
    private ToggleButton tbWed;
    private ToggleButton tbThu;
    private ToggleButton tbFri;
    private ToggleButton tbSat;
    private ToggleButton tbSun;
    private ArrayList<ToggleButton> alToggleButtons;
    private ImageButton enableDatePicker;
    DatePicker datePicker;

    LinearLayout layoutToggleButton;
    LinearLayout layoutDatePicker;

    //FOR EDIT
    long id_alarm=0;
    DataSource datasource;
    Alarm alarm;

    public DateFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity a) {
        super.onAttach(a);
        dataPasserDay = (OnDataPassDay) a;
        dataPasserDate = (OnDataPassDate) a;
        dataPasserWhich = (OnDataPassWhich) a;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        datasource = new DataSource(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_date, container, false);
        initToggleButtons(v);
        return v;
    }

    private void setDays(boolean enabled) {
        for (int i=0;i<alToggleButtons.size();i++){
            alToggleButtons.get(i).setClickable(enabled);
            alToggleButtons.get(i).setEnabled(enabled);
        }
    }

    private void setDate(boolean enabled) {
        if (enabled){
            enableDatePicker.setVisibility(View.GONE);
            datePicker.setVisibility(View.VISIBLE);
        }else{
            datePicker.setVisibility(View.GONE);
            enableDatePicker.setVisibility(View.VISIBLE);
        }


    }

    public void initToggleButtons(View v) {

        layoutToggleButton = (LinearLayout) v.findViewById(R.id.togglebutton_layout);
        layoutDatePicker = (LinearLayout) v.findViewById(R.id.datepicker_layout);
        datePicker = (DatePicker) v.findViewById(R.id.datePicker);
        //TOGGLE BUTTONS
        tbMon = (ToggleButton) v.findViewById(R.id.toggle_button_mon);
        tbTue = (ToggleButton) v.findViewById(R.id.toggle_button_tue);
        tbWed = (ToggleButton) v.findViewById(R.id.toggle_button_wed);
        tbThu = (ToggleButton) v.findViewById(R.id.toggle_button_thu);
        tbFri = (ToggleButton) v.findViewById(R.id.toggle_button_fri);
        tbSat = (ToggleButton) v.findViewById(R.id.toggle_button_sat);
        tbSun = (ToggleButton) v.findViewById(R.id.toggle_button_sun);
        //ENABLE DATEPICKER BUTTON
        enableDatePicker = (ImageButton)v.findViewById(R.id.enableDatePicker);

        alToggleButtons = new ArrayList<>();
        alToggleButtons.add(tbMon);
        alToggleButtons.add(tbTue);
        alToggleButtons.add(tbWed);
        alToggleButtons.add(tbThu);
        alToggleButtons.add(tbFri);
        alToggleButtons.add(tbSat);
        alToggleButtons.add(tbSun);

        tbMon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    passDataDay(tbMon, 2);
                } else {
                    passDataDay(tbMon, 2);
                }
            }
        });
        tbTue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    passDataDay(tbTue, 3);
                } else {
                    passDataDay(tbTue, 3);
                }
            }
        });
        tbWed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    passDataDay(tbWed, 4);
                } else {
                    passDataDay(tbWed, 4);
                }
            }
        });
        tbThu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    passDataDay(tbThu, 5);
                } else {
                    passDataDay(tbThu, 5);
                }
            }
        });
        tbFri.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    passDataDay(tbFri, 6);
                } else {
                    passDataDay(tbFri, 6);
                }
            }
        });
        tbSat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    passDataDay(tbSat, 7);
                } else {
                    passDataDay(tbSat, 7);
                }
            }
        });
        tbSun.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    passDataDay(tbSun, 1);
                } else {
                    passDataDay(tbSun, 1);
                }
            }
        });

        layoutToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDays(true);
                setDate(false);
                passDataWhich(1);
            }
        });

        enableDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDays(false);
                setDate(true);
                passDataWhich(2);
            }
        });
        Calendar cal = Calendar.getInstance();
        if(!getArguments().isEmpty()) {
            id_alarm = Long.parseLong(getArguments().getString("id_alarm").toString());
            datasource.open();
            alarm = datasource.getAlarmWithId(id_alarm);
            datasource.close();
            tbMon.setChecked(Boolean.parseBoolean(alarm.getIsMon()));
            tbTue.setChecked(Boolean.parseBoolean(alarm.getIsTue()));
            tbWed.setChecked(Boolean.parseBoolean(alarm.getIsWed()));
            tbThu.setChecked(Boolean.parseBoolean(alarm.getIsThu()));
            tbFri.setChecked(Boolean.parseBoolean(alarm.getIsFri()));
            tbSat.setChecked(Boolean.parseBoolean(alarm.getIsSat()));
            tbSun.setChecked(Boolean.parseBoolean(alarm.getIsSun()));
            cal.set(Integer.parseInt(alarm.getYear()),Integer.parseInt(alarm.getMonth()),Integer.parseInt(alarm.getDay()));
        }
        datePicker.init(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                passDataDate(datePicker);
            }
        });
        passDataDate(datePicker);

    }


    public void passDataDay(ToggleButton tbDay, int day) {
        dataPasserDay.onDataPassDay(tbDay, day);
    }

    public interface OnDataPassDay {
        public void onDataPassDay(ToggleButton tbDay, int day);
    }

    public void passDataDate(DatePicker datePicker) {
        dataPasserDate.onDataPassDate(datePicker);
    }

    public interface OnDataPassDate {
        public void onDataPassDate(DatePicker datePicker);
    }

    public void passDataWhich(int which) {
        dataPasserWhich.onDataPassWhich(which);
    }

    public interface OnDataPassWhich {
        public void onDataPassWhich(int which);
    }
}
