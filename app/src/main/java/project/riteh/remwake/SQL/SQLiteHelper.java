package project.riteh.remwake.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper {

    //INITIAL VALUES
    public static final String [] INIT_MUSIC_NAME = {"Default Phone","Nana - Lonely", "Funny Alarm", "Extreme Alarm", "Tick-Tock Alarm"};
    public static final String [] INIT_MUSIC_PATH = {"default", "nana-lonely_v5.mp3", "funny_alarm.mp3", "extreme_clock_alarm.mp3","tick_tock_alarm_clock_mp3"};

    //ALARM TABLE
    public static final String TABLE_NAME_ALARM = "alarm";
    public static final String COLUMN_ID_ALARM = "id_alarm";
    public static final String COLUMN_HOURS = "hours";
    public static final String COLUMN_MINUTES = "minutes";
    public static final String COLUMN_IS_TOGGLED = "is_toggled";
    public static final String COLUMN_IS_MONDAY = "is_monday";
    public static final String COLUMN_IS_TUESDAY = "is_tuesday";
    public static final String COLUMN_IS_WEDNESDAY = "is_wednesday";
    public static final String COLUMN_IS_THURSDAY = "is_thursday";
    public static final String COLUMN_IS_FRIDAY = "is_friday";
    public static final String COLUMN_IS_SATURDAY = "is_saturday";
    public static final String COLUMN_IS_SUNDAY = "is_sunday";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_MONTH = "month";
    public static final String COLUMN_DAY_OF_MONTH = "day_of_month";
    public static final String COLUMN_DAY_OR_DATE = "day_or_date";
    public static final String COLUMN_IS_SMART_RISE = "is_smart_rise";
    public static final String COLUMN_VIBRATION = "vibration";
    public static final String COLUMN_CHALLENGE = "challenge";

    //ALARM MUSIC TABLE
    public static final String TABLE_NAME_ALARM_MUSIC = "alarm_music";
    public static final String COLUMN_ID_ALARM_MUSIC = "id_alarm_music";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_MUSIC_PATH = "music_path";

    //ALARM WITH MUSIC TABLE
    public static final String TABLE_NAME_ALARM_WITH_MUSIC = "alarm_with_music";

    //REMINDER TABLE
    public static final String TABLE_NAME_REMINDER = "reminder";
    public static final String COLUMN_ID_REMINDER ="id_reminder";
    public static final String COLUMN_TITLE_REMINDER="title";
    public static final String COLUMN_TEXT_REMINDER="text";
    public static final String COLUMN_LONGITUDE="longitude";
    public static final String COLUMN_LATITUDE="latitude";
    public static final String COLUMN_ADDRESS="address";
    public static final String COLUMN_TYPE="type";
//    public static final String COLUMN_HOURS = "hours";
//    public static final String COLUMN_MINUTES = "minutes";
//    public static final String COLUMN_IS_TOGGLED = "is_toggled";
//    public static final String COLUMN_YEAR = "year";
//    public static final String COLUMN_MONTH = "month";
//    public static final String COLUMN_DAY_OF_MONTH = "day_of_month";


    //DATABASE SETTINGS
    private static final String DATABASE_NAME = "remwake.db";
    private static final int DATABASE_VERSION = 14;


    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE_ALARM);
        db.execSQL(CREATE_TABLE_REMINDER);
        db.execSQL(CREATE_TABLE_ALARM_MUSIC);
        db.execSQL(CREATE_TABLE_ALARM_WITH_MUSIC);
        insertValues(db);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_ALARM);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_REMINDER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_ALARM_MUSIC);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_ALARM_WITH_MUSIC);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    //CREATE TABLE ALARM
    private static final String CREATE_TABLE_ALARM = "create table "
            + TABLE_NAME_ALARM + "("
            + COLUMN_ID_ALARM + " integer primary key autoincrement, "
            + COLUMN_HOURS + " text not null, "
            + COLUMN_MINUTES + " text not null, "
            + COLUMN_IS_TOGGLED + " text, "
            + COLUMN_IS_MONDAY + " text, "
            + COLUMN_IS_TUESDAY + " text, "
            + COLUMN_IS_WEDNESDAY + " text, "
            + COLUMN_IS_THURSDAY + " text, "
            + COLUMN_IS_FRIDAY + " text, "
            + COLUMN_IS_SATURDAY + " text, "
            + COLUMN_IS_SUNDAY + " text, "
            + COLUMN_YEAR + " text, "
            + COLUMN_MONTH + " text, "
            + COLUMN_DAY_OF_MONTH + " text, "
            + COLUMN_DAY_OR_DATE + " text, "
            + COLUMN_IS_SMART_RISE + " text, "
            + COLUMN_VIBRATION + " text, "
            + COLUMN_CHALLENGE + " text);";

    //CREATE TABLE REMINDER
    private static final String CREATE_TABLE_REMINDER= "create table "
            + TABLE_NAME_REMINDER + "("
            + COLUMN_ID_REMINDER + " integer primary key autoincrement, "
            + COLUMN_TITLE_REMINDER + " text , "
            + COLUMN_TEXT_REMINDER + " text , "
            + COLUMN_LONGITUDE + " real , "
            + COLUMN_LATITUDE + " real , "
            + COLUMN_ADDRESS + " text , "
            + COLUMN_TYPE + " text , "
            + COLUMN_HOURS + " text , "
            + COLUMN_MINUTES + " text , "
            + COLUMN_IS_TOGGLED + " text, "
            + COLUMN_YEAR + " text, "
            + COLUMN_MONTH + " text, "
            + COLUMN_DAY_OF_MONTH + " text);";

    //CREATE TABLE MUSIC
    private static final String CREATE_TABLE_ALARM_MUSIC= "create table "
            + TABLE_NAME_ALARM_MUSIC + "("
            + COLUMN_ID_ALARM_MUSIC + " integer primary key autoincrement, "
            + COLUMN_NAME + " text unique, "
            + COLUMN_MUSIC_PATH + " text);";

    public void insertValues(SQLiteDatabase db) {
        //INSERT GROUP LIST
        for (int i = 0; i < INIT_MUSIC_NAME.length; i++) {
            db.execSQL("INSERT INTO " + TABLE_NAME_ALARM_MUSIC + "(" + COLUMN_NAME + ", " + COLUMN_MUSIC_PATH + ") VALUES('" + INIT_MUSIC_NAME[i] + "', '"+ INIT_MUSIC_PATH[i] + "');");
        }
    }

    //CREATE TABLE ALARM WITH MUSIC
    private static final String CREATE_TABLE_ALARM_WITH_MUSIC= "create table "
            + TABLE_NAME_ALARM_WITH_MUSIC + "("
            + COLUMN_ID_ALARM + " integer not null, "
            + COLUMN_ID_ALARM_MUSIC + " integer not null, "+
            "PRIMARY KEY("+COLUMN_ID_ALARM+", "+COLUMN_ID_ALARM_MUSIC+"));";

}
