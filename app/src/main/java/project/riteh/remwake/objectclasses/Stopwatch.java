package project.riteh.remwake.objectclasses;

public class Stopwatch {
    private long startTime = 0;
    private boolean running = false;
    private boolean pause = false;
    private long currentTime = 0;
    private int i = 0;

    public void start() {
        this.startTime = System.currentTimeMillis();
        this.running = true;
    }

    public void stop() {
        this.running = false;
        i=0;
        currentTime = 0;
        startTime = 0;
        this.pause = false;
    }

    public void pause() {
        this.running = false;
        this.pause = true;
        currentTime = System.currentTimeMillis() - startTime;
    }
    public void resume() {
        this.running = true;
        this.pause =false;
        this.startTime = System.currentTimeMillis() - currentTime;
    }

    //elapsed time in milliseconds
    public long getElapsedTimeMilli() {
        long elapsed = 0;
        if (running) {
            elapsed =(System.currentTimeMillis() - startTime) % 1000;
        }else if (pause){
            elapsed =currentTime % 1000;
        }
        return elapsed;
    }

    //elapsed time in seconds
    public long getElapsedTimeSecs() {
        long elapsed = 0;
        if (running) {
            elapsed = ((System.currentTimeMillis() - startTime) / 1000) % 60;
        }else if (pause){
            elapsed =(currentTime / 1000) % 60;
        }
        return elapsed;
    }

    //elapsed time in minutes
    public long getElapsedTimeMin() {
        long elapsed = 0;
        if (running) {
            elapsed = (((System.currentTimeMillis() - startTime) / 1000) / 60 ) % 60;
        }else if (pause){
            elapsed =((currentTime / 1000) / 60 ) % 60;
        }
        return elapsed;
    }

    //elapsed time in hours
    public long getElapsedTimeHour() {
        long elapsed = 0;
        if (running) {
            elapsed = ((((System.currentTimeMillis() - startTime) / 1000) / 60 ) / 60);
        }else if (pause){
            elapsed =(((currentTime / 1000) / 60 ) / 60);
        }
        return elapsed;
    }

    public String toString() {
        return getElapsedTimeHour() + ":" + getElapsedTimeMin() + ":"
                + getElapsedTimeSecs() + "." + getElapsedTimeMilli();
    }
    public String toStringStopwatch() {
        return "" + String.format("%02d", getElapsedTimeMin()) + ":"
                + String.format("%02d", getElapsedTimeSecs()) + "." + String.format("%03d", getElapsedTimeMilli());
    }
    public String toStringStopwatchSplit() {
        i++;
        if (pause) {
            return "" + String.format("(" + i + ") %02d", getElapsedTimeMin()) + ":"
                    + String.format("%02d", getElapsedTimeSecs()) + "." + String.format("%03d", getElapsedTimeMilli());
        } else {
            return "" + String.format("(" + i + ") %02d", getElapsedTimeMin()) + ":"
                    + String.format("%02d", getElapsedTimeSecs()) + "." + String.format("%03d", getElapsedTimeMilli());
        }
    }
}
