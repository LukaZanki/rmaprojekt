package project.riteh.remwake.objectclasses;

public class Reminder {
    public String id;
    public String title = "";
    public String text = "";
    public double longitude;
    public double latitude;
    public String address = "";
    public String hours = "";
    public String minutes = "";
    public String is_toggled = "false";
    public String year = "";
    public String month = "";
    public String day_of_month = "";
    public String type = "0";

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }

    public String getIs_toggled() {
        return is_toggled;
    }

    public void setIs_toggled(String is_toggled) {
        this.is_toggled = is_toggled;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay_of_month() {
        return day_of_month;
    }

    public void setDay_of_month(String day_of_month) {
        this.day_of_month = day_of_month;
    }

    public Reminder(String title, String text) {
        this.title = title;
        this.text = text;
    }
    public Reminder(){}

    public void setTimeDate(int hour, int minute,int year,int month,int dayofmonth, String address) {
        this.hours = ""+hour;
        this.minutes = ""+minute;
        this.year = ""+year;
        this.month = ""+month;
        this.day_of_month = ""+dayofmonth;
        this.address = address;
    }

    public void setAllOptions(String emptyString,String type) {
        this.longitude = 0;
        this.latitude = 0;
        this.hours = emptyString;
        this.minutes = emptyString;
        this.year = emptyString;
        this.month = emptyString;
        this.day_of_month = emptyString;
        this.address = emptyString;
        this.type = type;
    }

    public void setPlace(double longitude, double latitude, String address) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.address = address;
    }
}
