package project.riteh.remwake.objectclasses;

public class AlarmWithMusic {
    Alarm alarm;
    Music music;

    public AlarmWithMusic(){

    }

    public Alarm getAlarm() {
        return alarm;
    }

    public void setAlarm(Alarm alarm) {
        this.alarm = alarm;
    }

    public Music getMusic() {
        return music;
    }

    public void setMusic(Music music) {
        this.music = music;
    }
}
