package project.riteh.remwake.objectclasses;

public class Alarm {
    public String idalarm = "";
    public String hours = "";
    public String minutes = "";
    public String is_toggled = "false";
    public String isMon ="false";
    public String isTue ="false";
    public String isWed ="false";
    public String isThu ="false";
    public String isFri ="false";
    public String isSat ="false";
    public String isSun ="false";
    public String year = "";
    public String month = "";
    public String day = "";
    public String isDayOrDate = "1";
    public String smartRise = "false";
    public String vibration = "1";
    public String challenge = "1";

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getIsDayOrDate() {
        return isDayOrDate;
    }

    public void setIsDayOrDate(String isDayOrDate) {
        this.isDayOrDate = isDayOrDate;
    }

    public String getSmartRise() {
        return smartRise;
    }

    public void setSmartRise(String smartRise) {
        this.smartRise = smartRise;
    }

    public String getVibration() {
        return vibration;
    }

    public void setVibration(String vibration) {
        this.vibration = vibration;
    }

    public String getChallenge() {
        return challenge;
    }

    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }

    public String getIs_toggled() {
        return is_toggled;
    }

    public void setIs_toggled(String is_toggled) {
        this.is_toggled = is_toggled;
    }

    public String getIsMon() {
        return isMon;
    }

    public void setIsMon(String isMon) {
        this.isMon = isMon;
    }

    public String getIsTue() {
        return isTue;
    }

    public void setIsTue(String isTue) {
        this.isTue = isTue;
    }

    public String getIsWed() {
        return isWed;
    }

    public void setIsWed(String isWed) {
        this.isWed = isWed;
    }

    public String getIsThu() {
        return isThu;
    }

    public void setIsThu(String isThu) {
        this.isThu = isThu;
    }

    public String getIsFri() {
        return isFri;
    }

    public void setIsFri(String isFri) {
        this.isFri = isFri;
    }

    public String getIsSat() {
        return isSat;
    }

    public void setIsSat(String isSat) {
        this.isSat = isSat;
    }

    public String getIsSun() {
        return isSun;
    }

    public void setIsSun(String isSun) {
        this.isSun = isSun;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }

    public String getisToggled() {
        return is_toggled;
    }

    public void setisToggled(String is_toggled) {
        this.is_toggled = is_toggled;
    }

    public String getIDAlarm() {
        return idalarm;
    }

    public void setIDAlarm(String idalarm) {
        this.idalarm = idalarm;
    }

    //EMPTY CONSTRUCTOR
    public Alarm(){
    }

    public String getFormatedHours (){
        return formatTime(hours);
    }
    public String getFormatedMinutes (){
        return formatTime(minutes);
    }
    public String getFormatedDay (){
        return formatTime(day);
    }
    public String getFormatedMonth (){
        return formatTime(month);
    }

    public String formatTime(String time){
        if (!time.equals("")) {
            if (Integer.parseInt(time) < 10) {
                time = "0" + time;
            }
        }
        return time;
    }

}
