package project.riteh.remwake;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import net.frakbot.glowpadbackport.GlowPadView;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import project.riteh.remwake.alarmservice.AlarmReceiver;
import project.riteh.remwake.mathgenerator.RandomMathQuestionGenerator;
import project.riteh.remwake.objectclasses.Alarm;
import project.riteh.remwake.objectclasses.AlarmWithMusic;
import project.riteh.remwake.sql.DataSource;

public class AlarmTriggerActivity extends AppCompatActivity implements SensorListener{

    private MediaPlayer mMediaPlayer;
    private DataSource dataSource;
    private Alarm alarm = new Alarm();
    private ArrayList<AlarmWithMusic> alarmWithMusicList = new ArrayList<>();
    AlarmReceiver alarmReceiver;
    Vibrator vibrator;
    Calendar calCurrent;
    //SHAKE VARIABLES
    SensorManager sensorManager;
    private static final int SHAKE_THRESHOLD = 1000;
    private static final int TIME_THRESHOLD = 100;
    private static final int SHAKE_TIMEOUT = 500;
    private static final int SHAKE_DURATION = 8000;
    private static final int SHAKE_COUNT = 10;
    private float mLastX=-1.0f, mLastY=-1.0f, mLastZ=-1.0f;
    private long mLastTime;
    private int mShakeCount = 0;
    private long mLastShake;
    private long mLastForce;
    //MATH VARIABLES
    private ViewSwitcher viewSwitcher;
    private GlowPadView glowPad;
    private TextView tvFormula;
    private EditText etResult;

    private GlowPadView glowPadMath;

    int volume = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_alarm_trigger);
        Intent i = getIntent();
        String id_alarm = i.getStringExtra("id_alarm");

        final AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        volume = audioManager.getStreamVolume(AudioManager.STREAM_ALARM);
        viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcherAlarmTrigger);
        glowPad = (GlowPadView) findViewById(R.id.GlowPad);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Log.i("TRIGGER", id_alarm);
        dataSource = new DataSource(getApplicationContext());
        dataSource.open();
        alarm = dataSource.getAlarmWithId(Integer.parseInt(id_alarm));
        alarmWithMusicList = dataSource.getAllMusicWithAlarmID(Integer.parseInt(id_alarm));
        dataSource.close();
        TextView textViewAlarm = (TextView) findViewById(R.id.currentTimeAlarm);
        TextView textView = (TextView) findViewById(R.id.currentTime);
        alarmReceiver = new AlarmReceiver();

        //SEND NOTIFICATION TO WATCH
        sendNotificationToWatch();
        //

        //RING INIT
        setListenerGlowPadView();

        if (Boolean.parseBoolean(alarm.getSmartRise()) && checkIfSmartRise()){
            textViewAlarm.setText(calCurrent.get(Calendar.HOUR_OF_DAY) + ":" + calCurrent.get(Calendar.MINUTE));
            smartRiseMusic();
        }else {

            textViewAlarm.setText(alarm.getFormatedHours() + ":" + alarm.getFormatedMinutes());
            textView.setText(alarm.getFormatedHours() + ":" + alarm.getFormatedMinutes());

            if (alarm.getVibration().equals("1")) {
                getAlarmMusicPath();
            } else if (alarm.getVibration().equals("2")) {
                vibration();
                getAlarmMusicPath();
            } else if (alarm.getVibration().equals("3")) {
                vibration();
            }
            Log.i("TRIGGER", "VIBRATION: " + alarm.getVibration());

            if (alarm.getChallenge().equals("2") || alarm.getChallenge().equals("3")) {
                glowPad.setVisibility(View.INVISIBLE);
                if (alarm.getChallenge().equals("2")) {
                    shakeChallenge();
                } else if (alarm.getChallenge().equals("3")) {
                    viewSwitcher.showNext();
                    mathChallenge();
                }
            }
        }
    }

    private void smartRiseMusic() {
        mMediaPlayer = new MediaPlayer();
        try {
            AssetFileDescriptor descriptor = this.getAssets().openFd("jeremy_soule_sylvari.mp3");
            long start = descriptor.getStartOffset();
            long end = descriptor.getLength();
            mMediaPlayer.setDataSource(descriptor.getFileDescriptor(), start, end);
            final AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_ALARM,2,2);
            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.setLooping(false);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
                mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        audioManager.setStreamVolume(AudioManager.STREAM_ALARM,volume,volume);
                        finish();
                    }
                });
            }

        } catch (IOException e) {
            System.out.println("OOPS");
        }
    }

    private boolean checkIfSmartRise() {
        calCurrent = Calendar.getInstance();
        Calendar calAlarm = Calendar.getInstance();
        calAlarm.set(Calendar.HOUR_OF_DAY, Integer.parseInt(alarm.getHours()));
        calAlarm.set(Calendar.MINUTE, Integer.parseInt(alarm.getMinutes()));
        calAlarm.set(Calendar.SECOND, 00);
        if (calCurrent.getTimeInMillis()<(calAlarm.getTimeInMillis()-900000)){
            return true;
        }
        return false;
    }

    private void shakeChallenge() {
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this,SensorManager.SENSOR_ACCELEROMETER,SensorManager.SENSOR_DELAY_GAME);
    }

    private void unregisterShakeChallenge() {
        sensorManager.unregisterListener(this);
        sensorManager = null;
    }

    private void mathChallenge() {
        tvFormula = (TextView) findViewById(R.id.tvExpression);
        etResult = (EditText) findViewById(R.id.inputMathNumber);
        glowPadMath = (GlowPadView) findViewById(R.id.GlowPadMathPuzzle);
        setImageButtonListeners();
        generateFunction();
    }

    private void setImageButtonListeners() {
        glowPadMath.setOnTriggerListener(new GlowPadView.OnTriggerListener() {
            @Override
            public void onGrabbed(View v, int handle) {
                // Do nothing
            }

            @Override
            public void onReleased(View v, int handle) {
                // Do nothing
            }

            @Override
            public void onTrigger(View v, int target) {
                //Toast.makeText(getApplicationContext(), "Target triggered! ID=" + target, Toast.LENGTH_SHORT).show();


                if (target == 2) { //STOP
                    if (calculateFunction()){
                        //REMOVE NOTIFICATION FROM WATCH
                        removeNotificationFromWatch();
                        //
                        stopAlarm();
                    } else{
                        Toast.makeText(AlarmTriggerActivity.this, "No, you are wrong! Try again! Wake up!", Toast.LENGTH_SHORT).show();
                    }
                }

                if (target == 0) { //SNOOZE

                    if (calculateFunction()){
                        //REMOVE NOTIFICATION FROM WATCH
                        removeNotificationFromWatch();
                        //
                        snoozeAlarm();
                        stopAlarm();
                    } else{
                        Toast.makeText(AlarmTriggerActivity.this, "No, you are wrong! Try again! Wake up!", Toast.LENGTH_SHORT).show();
                    }
                }
                glowPad.reset(true);
            }

            @Override
            public void onGrabbedStateChange(View v, int handle) {
                // Do nothing
            }

            @Override
            public void onFinishFinalAnimation() {
                // Do nothing
            }
        });
    }

    private void snoozeAlarm() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 10); //SNOOZE = 10 minutes;
        alarmReceiver.SetAlarm(getApplicationContext(), calendar, Integer.parseInt(alarm.getIDAlarm()));
    }

    private void setListenerGlowPadView() {
        glowPad.setOnTriggerListener(new GlowPadView.OnTriggerListener() {
            @Override
            public void onGrabbed(View v, int handle) {
                // Do nothing
            }

            @Override
            public void onReleased(View v, int handle) {
                // Do nothing
            }

            @Override
            public void onTrigger(View v, int target) {
                //Toast.makeText(getApplicationContext(), "Target triggered! ID=" + target, Toast.LENGTH_SHORT).show();


                if (target == 2) { //STOP
                    //REMOVE NOTIFICATION FROM WATCH
                    removeNotificationFromWatch();
                    //
                    stopAlarm();
                }

                if (target == 0) { //SNOOZE
                    //REMOVE NOTIFICATION FROM WATCH
                    removeNotificationFromWatch();
                    //
                    snoozeAlarm();
                    stopAlarm();
                }
                glowPad.reset(true);
            }

            @Override
            public void onGrabbedStateChange(View v, int handle) {
                // Do nothing
            }

            @Override
            public void onFinishFinalAnimation() {
                // Do nothing
            }
        });
    }

    private void vibration() {
        long[] pattern = {0, 1000, 500};
        vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(pattern, 0);
    }

    private void playSound(Context context, Uri alert) {
        mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(context, alert);
            final AudioManager audioManager = (AudioManager) context
                    .getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.setLooping(true);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }
        } catch (IOException e) {
            System.out.println("OOPS");
        }
    }

    public void playAsset(String fileName) {
        mMediaPlayer = new MediaPlayer();
        try {
            AssetFileDescriptor descriptor = this.getAssets().openFd(fileName);
            long start = descriptor.getStartOffset();
            long end = descriptor.getLength();
            mMediaPlayer.setDataSource(descriptor.getFileDescriptor(), start, end);
            final AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.setLooping(true);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }
        } catch (IOException e) {
            System.out.println("OOPS");
        }
    }

    //Get an alarm sound. Try for an alarm. If none set, try notification,
    //Otherwise, ringtone.
    private void getAlarmMusicPath() {
        Random r = new Random();
        int randomMusic = 1;
        String music_path = "default";
        if (alarmWithMusicList.size()!=0) {
            randomMusic = r.nextInt(alarmWithMusicList.size());
            music_path = alarmWithMusicList.get(randomMusic).getMusic().getMusic_path();
        }

        if (!music_path.equals("default")) {
            playAsset(music_path);
        } else if (music_path.equals("default")) {
            Uri defaultUri = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_ALARM);
            if (defaultUri == null) {
                defaultUri = RingtoneManager
                        .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                if (defaultUri == null) {
                    defaultUri = RingtoneManager
                            .getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                }
            }
            playSound(this, defaultUri);
        }

    }

    private void generateFunction() {
        RandomMathQuestionGenerator rmqG = new RandomMathQuestionGenerator();

        String expression = rmqG.getGeneratedRandomQuestions();

        tvFormula.setText(expression);
    }

    private boolean calculateFunction(){
        String fun = tvFormula.getText().toString();
        Expression calc = new ExpressionBuilder(fun).build();
        double result =calc.evaluate();

        Log.i("TRIGGER", "EXP4J RESULT: "+etResult.getText().toString()+ "   EDIT TEXT: "+etResult.getText().toString());

        if(etResult.getText().toString().equals(Integer.toString((int)result))){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void onSensorChanged(int sensor, float[] values) {
        if (sensor != SensorManager.SENSOR_ACCELEROMETER) return;
        long now = System.currentTimeMillis();

        if ((now - mLastForce) > SHAKE_TIMEOUT) {
            mShakeCount = 0;
        }

        if ((now - mLastTime) > TIME_THRESHOLD) {
            long diff = now - mLastTime;
            float speed = Math.abs(values[SensorManager.DATA_X] + values[SensorManager.DATA_Y] + values[SensorManager.DATA_Z] - mLastX - mLastY - mLastZ) / diff * 10000;
            if (speed > SHAKE_THRESHOLD) {
                if ((++mShakeCount >= SHAKE_COUNT) && (now - mLastShake > SHAKE_DURATION)) {
                    mLastShake = now;
                    mShakeCount = 0;
                    enableAlarmStop();
                }
                mLastForce = now;
            }
            mLastTime = now;
            mLastX = values[SensorManager.DATA_X];
            mLastY = values[SensorManager.DATA_Y];
            mLastZ = values[SensorManager.DATA_Z];
        }
    }

    private void enableAlarmStop() {
        if (sensorManager!=null){
            unregisterShakeChallenge();
        }
        glowPad.setVisibility(View.VISIBLE);
    }

    private void stopAlarm(){
        if (vibrator != null) {
            vibrator.cancel();
        }
        if (mMediaPlayer != null) {
            if (mMediaPlayer.isLooping()) {
                mMediaPlayer.setLooping(false);
            }
            mMediaPlayer.stop();
            final AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_ALARM,volume,volume);
        }
        finish();
    }

    @Override
    public void onAccuracyChanged(int sensor, int accuracy) {

    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        final AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_ALARM,volume,volume);
        removeNotificationFromWatch();
        stopAlarm();
    }

    private void sendNotificationToWatch() {
        // Create a WearableExtender to add functionality for wearables
        NotificationCompat.WearableExtender wearableExtender =
                new NotificationCompat.WearableExtender()
                        .setHintHideIcon(true);

        int notificationId = 1;

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("RemWake")
                        .setContentText("Alarm: "+alarm.getFormatedHours()+":"+ alarm.getFormatedMinutes())
                        .extend(wearableExtender)
                        .setAutoCancel(true);

// Get an instance of the NotificationManager service
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);

// Build the notification and issues it with notification manager.
        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    private void removeNotificationFromWatch() {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) this.getSystemService(ns);
        nMgr.cancel(1);
    }
}