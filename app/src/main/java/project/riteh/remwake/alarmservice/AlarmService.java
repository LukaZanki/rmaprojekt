package project.riteh.remwake.alarmservice;

import android.app.IntentService;
import android.content.Intent;
import project.riteh.remwake.AlarmTriggerActivity;

public class AlarmService extends IntentService {

    public AlarmService() {
        super("AlarmService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Intent i = new Intent(this, AlarmTriggerActivity.class);
        i.putExtra("id_alarm", intent.getStringExtra("id_alarm"));
        i.putExtra("type", "nothing");
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }
}
