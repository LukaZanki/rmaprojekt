package project.riteh.remwake.alarmservice;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import java.util.Calendar;

public class AlarmReceiver extends WakefulBroadcastReceiver {

    private static int id_alarm;

    @Override
    public void onReceive(Context context, Intent intent) {
        //CALL SERVICE CLASS
        Intent serviceIntent = new Intent(context, AlarmService.class);
        serviceIntent.putExtra("id_alarm", ""+id_alarm);
        startWakefulService(context, serviceIntent);
        //CHANGE RESULT CODE
        setResultCode(Activity.RESULT_OK);
    }

    public void SetAlarm(Context context, Calendar calendar, int id)
    {
        id_alarm = id;
        AlarmManager am =( AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, AlarmReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, id, i, 0);
        am.setAlarmClock (new AlarmManager.AlarmClockInfo(calendar.getTimeInMillis(), pi), pi);
    }

    public void CancelAlarm(Context context, int id)
    {
        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, id, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
}
