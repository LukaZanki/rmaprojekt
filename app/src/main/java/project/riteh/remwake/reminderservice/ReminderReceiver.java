package project.riteh.remwake.reminderservice;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.Toast;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import java.util.Calendar;
import project.riteh.remwake.R;
import project.riteh.remwake.objectclasses.Reminder;

public class ReminderReceiver extends WakefulBroadcastReceiver {

    private static int id_reminder;
    private static String title,text;
    private Reminder reminder=null;
    private GoogleApiClient mGoogleApiClient=null;
    private Context mContext=null;
    private PendingIntent mGeofencePendingIntent;
    protected Geofence mGeofence;

    @Override
    public void onReceive(Context context, Intent intent) {
        //CALL SERVICE CLASS
        Intent serviceIntent = new Intent(context, ReminderService.class);
        serviceIntent.putExtra("id_reminder", ""+id_reminder);
        serviceIntent.putExtra("title", title);
        serviceIntent.putExtra("text", text);
        startWakefulService(context, serviceIntent);
        //CHANGE RESULT CODE
        setResultCode(Activity.RESULT_OK);
    }

    //ALARM
    public void SetReminder(Context context, Calendar calendar, int id, String title, String text)
    {
        id_reminder = id;
        this.title = title;
        this.text=text;
        AlarmManager am =(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, ReminderReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, id, i, 0);
        am.setAlarmClock (new AlarmManager.AlarmClockInfo(calendar.getTimeInMillis(), pi), pi);
    }

    public void CancelReminder(Context context, int id)
    {
        Intent intent = new Intent(context, ReminderReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, id, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    public void setGeofencingParameteres(Reminder reminder, GoogleApiClient mGoogleApiClient,Context mContext){
        this.reminder = reminder;
        this.mGoogleApiClient = mGoogleApiClient;
        this.mContext = mContext;
    }

    //GEOFENCE
    public void setGeofence() {
        // Build a new Geofence object
        this.mGeofence = new Geofence.Builder()
                .setRequestId(reminder.getId())
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                .setCircularRegion(reminder.getLatitude(), reminder.getLongitude(), R.string.circular_area)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .build();
    }

    public void startGeofence(){
        if (!mGoogleApiClient.isConnected()) {
            Toast.makeText(mContext, "Not connected to Google map API", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            LocationServices.GeofencingApi.addGeofences(
                    mGoogleApiClient,
                    // The GeofenceRequest object.
                    getGeofencingRequest(),
                    // A pending intent that that is reused when calling removeGeofences(). This
                    // pending intent is used to generate an intent when a matched geofence
                    // transition is observed.
                    getGeofencePendingIntent()
            ); // Result processed in onResult().
        } catch (SecurityException securityException) {
            // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
            logSecurityException(securityException);
        }
    }

    public void removeGeofences() {
        if (!mGoogleApiClient.isConnected()) {
            Toast.makeText(mContext, "Not connected to Google map API", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            // Remove geofences.
            LocationServices.GeofencingApi.removeGeofences(
                    mGoogleApiClient,
                    // This is the same pending intent that was used in addGeofences().
                    getGeofencePendingIntent()
            ); // Result processed in onResult().
        } catch (SecurityException securityException) {
            // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
            logSecurityException(securityException);
        }
    }

        private PendingIntent getGeofencePendingIntent() {
            // Reuse the PendingIntent if we already have it.
            if (mGeofencePendingIntent != null) {
                return mGeofencePendingIntent;
            }
            Intent intent = new Intent(mContext, GeofenceTransitionsIntentService.class);
            intent.putExtra("id_reminder", reminder.getId());
            intent.putExtra("title", reminder.getTitle());
            intent.putExtra("text", reminder.getText());
            // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
            // addGeofences() and removeGeofences().
            return PendingIntent.getService(mContext, Integer.parseInt(reminder.getId()), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
        // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
        // is already inside that geofence.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);

        // Add the geofences to be monitored by geofencing service.
        builder.addGeofence(mGeofence);

        // Return a GeofencingRequest.
        return builder.build();
    }

    private void logSecurityException(SecurityException securityException) {
        Log.e("REMINDER ADAPTER", "Invalid location permission. " +
                "You need to use ACCESS_FINE_LOCATION with geofences", securityException);
    }
}