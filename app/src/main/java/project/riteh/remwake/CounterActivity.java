package project.riteh.remwake;

import android.app.DialogFragment;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import at.grabner.circleprogress.CircleProgressView;

public class CounterActivity extends AppCompatActivity {

    private ImageButton startButton;
    private ImageButton pauseButton;
    private ImageButton restartButton;
    private ImageButton editButton;
    private TextView npMinute;
    private TextView npSecond;
    private CircleProgressView circleView;
    private int prevSecValue=0;
    private float percent_done=100;
    private float circleValue=0;
    private int minutes;
    private int seconds;

   private CountDownTimer timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counter);

        setButtonListeners();

        //numberpickers
         npMinute = (TextView) findViewById(R.id.tvMinute);
         npSecond = (TextView) findViewById(R.id.tvSecond);


        //circle bar
        circleView = (CircleProgressView) findViewById(R.id.circleView);

        circleView.setMaxValue(60);
        setCircleListener();


    }

    private void setButtonListeners() {
        //Button init
        pauseButton = (ImageButton)findViewById(R.id.pauseButton);
        startButton = (ImageButton) findViewById(R.id.startButton);
        restartButton=(ImageButton) findViewById(R.id.resetButton);
        editButton =(ImageButton) findViewById(R.id.editButton);
        //Layout init
        //Start button
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startButton.setVisibility(View.INVISIBLE);
                pauseButton.setVisibility(View.VISIBLE);
                restartButton.setVisibility(View.INVISIBLE);
              circleView.setSeekModeEnabled(false);
                //remove listener
                circleView.setOnProgressChangedListener(null);
                circleView.setMaxValue(100);
                editButton.setVisibility(View.INVISIBLE);

                startTimer();

            }
        });

        //Stop button
        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();

                pauseButton.setVisibility(View.INVISIBLE);
                startButton.setVisibility(View.VISIBLE);
                restartButton.setVisibility(View.VISIBLE);
                circleView.setMaxValue(60);
                percent_done=circleValue;



            }
        });

        restartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                npMinute.setText(String.format("%02d",0));
                npSecond.setText(String.format("%02d",0));
                circleView.setValueAnimated(0,100);
                percent_done=100;
                circleView.setSeekModeEnabled(true);
                setCircleListener();
                editButton.setVisibility(View.VISIBLE);

            }
        });

        editButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                DialogFragment newFragment = new TimerEditDialog();
                newFragment.show(getFragmentManager(), "dialog");



            }
        });
    }

    private void startTimer(){
        minutes=Integer.parseInt(npMinute.getText().toString());
        seconds = Integer.parseInt(npSecond.getText().toString());
        if(minutes == 0 && seconds== 0){
            DialogFragment newFragment = new TimerEditDialog();
            newFragment.show(getFragmentManager(), "dialog");

            //
            pauseButton.setVisibility(View.INVISIBLE);
            startButton.setVisibility(View.VISIBLE);
            restartButton.setVisibility(View.VISIBLE);
            circleView.setSeekModeEnabled(true);
            setCircleListener();
            circleView.setMaxValue(60);

            return;
        }
        final int total_msecs= ((minutes*60*1000+seconds*1000)+100);
        timer =new CountDownTimer(total_msecs, 50) {

            public void onTick(long millisUntilFinished) {

                circleValue=(millisUntilFinished/(float)total_msecs*percent_done);
                circleView.setValue(circleValue);

                int minutes=(int)((millisUntilFinished /(double)1000) / 60);
                int seconds= (int)Math.round(((millisUntilFinished /(double)1000 ) % 60));
                npMinute.setText(String.format("%02d",(minutes)));
                npSecond.setText(String.format("%02d",(seconds)));

            }

            public void onFinish() {
                npMinute.setText(String.format("%02d",0));
                npSecond.setText(String.format("%02d",0));
                percent_done=100;
                circleView.setMaxValue(60);
                circleView.setValue(60);
                circleView.setSeekModeEnabled(true);
                setCircleListener();
                //buttons toogle
                pauseButton.setVisibility(View.INVISIBLE);
                startButton.setVisibility(View.VISIBLE);
                restartButton.setVisibility(View.VISIBLE);
                //PLAY SOUND

                final AudioManager mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC), 0);

                MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.timer);
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){
                    int count=0;
                    @Override
                    public void onCompletion(MediaPlayer mp) {

                        if(count<2)
                            mp.start();
                        count++;

                    }});
                mp.start();
            }
        };
        timer.start();

    }


    private void setCircleListener(){
        circleView.setOnProgressChangedListener(new CircleProgressView.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(float value) {
                minutes=Integer.parseInt(npMinute.getText().toString().trim());
                seconds = Integer.parseInt(npSecond.getText().toString().trim());
                npSecond.setText(String.format("%02d",(int)value));
                if(prevSecValue>50 && value>0 && value<10)
                    if(minutes+1>60)
                        npMinute.setText(String.format("%02d",0));
                    else
                        npMinute.setText(String.format("%02d",(minutes+1)));
                else if (prevSecValue>=0 && prevSecValue<=5 && value>50 && value<60)
                    if(minutes-1>0)
                        npMinute.setText(String.format("%02d",(minutes-1)));
                    else
                        npMinute.setText(String.format("%02d",0));

                 npSecond.setText(String.format("%02d",(int)value));

                prevSecValue=(int)value;
            }
        });
    }
}


