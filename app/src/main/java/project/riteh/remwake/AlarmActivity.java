package project.riteh.remwake;

import android.content.Intent;
import android.graphics.Outline;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import project.riteh.remwake.adapters.AlarmAdapter;
import project.riteh.remwake.alarmservice.AlarmReceiver;
import project.riteh.remwake.objectclasses.Alarm;
import project.riteh.remwake.sql.DataSource;

public class AlarmActivity extends AppCompatActivity {

    private ArrayList<Alarm> alarmList;
    private RecyclerView rv;
    TextView tvEmptyView;
    Intent intentCreateAlarm = new Intent();
    DataSource datasource = null;
    AlarmReceiver alarmReceiver;
    AlarmAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        datasource = new DataSource(getApplicationContext());
        intentCreateAlarm = new Intent(this, CreateNewAlarmActivity.class);
        alarmReceiver = new AlarmReceiver();

        //RECYCLE VIEW
        rv = (RecyclerView)findViewById(R.id.rv);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(mLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());

        tvEmptyView = (TextView) findViewById(R.id.empty_view);

        setFloatingButton();
        initializeData();
        initializeAdapter();
        checkValues();

    }

    private void checkValues() {
        if (alarmList.isEmpty()) {
            rv.setVisibility(View.GONE);
            tvEmptyView.setVisibility(View.VISIBLE);

        } else {
            rv.setVisibility(View.VISIBLE);
            tvEmptyView.setVisibility(View.GONE);
        }
    }

    private void setFloatingButton() {
        ImageButton addButton = (ImageButton) findViewById(R.id.add_button);
        addButton.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                int diameter = getResources().getDimensionPixelSize(R.dimen.diameter);
                outline.setOval(0, 0, diameter, diameter);
            }
        });
        addButton.setClipToOutline(true);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intentCreateAlarm);

            }
        });
    }

    private void initializeData(){
        datasource.open();
        alarmList = datasource.getAllAlarms();
        datasource.close();
    }

    private void initializeAdapter(){
        adapter = new AlarmAdapter(AlarmActivity.this, alarmList, datasource);
        rv.setAdapter(adapter);
        instantiateItemTouchCallback();
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeData();
        checkValues();
        initializeAdapter();
    }

    public void instantiateItemTouchCallback(){
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder1) {
                return true;// true if moved
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                // remove from adapter
                int position = viewHolder.getAdapterPosition();
                Alarm alarm = alarmList.get(position);
                //IF ACTIVATED ALARM IS DELETED CANCEL IT AS WELL
                Switch toggleAlarm = (Switch)viewHolder.itemView.findViewById(R.id.toggle_alarm);
                TextView tvHours = (TextView)viewHolder.itemView.findViewById(R.id.hours);
                TextView tvMinutes = (TextView)viewHolder.itemView.findViewById(R.id.minutes);
                if (toggleAlarm.isChecked()){
                    alarmReceiver.CancelAlarm(AlarmActivity.this, Integer.parseInt(alarm.getIDAlarm()));
                }
                alarmList.remove(position);
                adapter.notifyItemRemoved(position);
                adapter.notifyItemRangeChanged(position, alarmList.size());
                Toast.makeText(AlarmActivity.this, "Deleted " + tvHours.getText().toString()+":"+ tvMinutes.getText().toString()+" alarm", Toast.LENGTH_SHORT).show();
                datasource.open();
                datasource.deleteAlarm(Long.parseLong(alarm.getIDAlarm()));
                datasource.deleteAlarmWithMusic(Long.parseLong(alarm.getIDAlarm()));
                alarmList = datasource.getAllAlarms();
                datasource.close();
                checkValues();
                initializeAdapter();
                if (alarmList.isEmpty()){

                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);

        itemTouchHelper.attachToRecyclerView(rv);
    }
}