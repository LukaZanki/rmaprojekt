package project.riteh.remwake;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.ToggleButton;
import java.util.ArrayList;
import project.riteh.remwake.adapters.CreateAlarmAdapter;
import project.riteh.remwake.fragments.DateFragment;
import project.riteh.remwake.fragments.OptionsFragment;
import project.riteh.remwake.fragments.TimeFragment;
import project.riteh.remwake.objectclasses.Alarm;
import project.riteh.remwake.objectclasses.Music;
import project.riteh.remwake.sql.DataSource;

public class CreateNewAlarmActivity extends AppCompatActivity implements TimeFragment.OnDataPass, DateFragment.OnDataPassDay, DateFragment.OnDataPassDate, DateFragment.OnDataPassWhich, OptionsFragment.OnDataPassOptionsSmartRise,  OptionsFragment.OnDataPassOptionsVibration,  OptionsFragment.OnDataPassOptionsChallenge,  OptionsFragment.OnDataPassOptionsMusic{

    private DataSource datasource = null;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    Alarm alarm = new Alarm();
    ArrayList<Music> musicArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_alarm);
        datasource = new DataSource(getApplicationContext());

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setListeners();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_alarm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_accept) {
            //TODO
            saveAlarm();
            finish();
            return true;

        }
        else if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveAlarm() {
        datasource.open();
        long alarm_id = datasource.createAlarm(""+alarm.getHours(), "" + alarm.getMinutes(), alarm.getIs_toggled(), alarm.getIsMon(), alarm.getIsTue(), alarm.getIsWed(), alarm.getIsThu(),alarm.getIsFri(),alarm.getIsSat(),alarm.getIsSun(),alarm.getYear(),alarm.getMonth(),alarm.getDay(),alarm.getIsDayOrDate(), alarm.getSmartRise(),alarm.getVibration(),alarm.getChallenge());
        if (musicArrayList==null){
            datasource.createAlarmWithMusic((int)alarm_id, 1); //music id 1 equals default
        }else {
            for (int i = 0; i < musicArrayList.size(); i++) {
                datasource.createAlarmWithMusic((int) alarm_id, Integer.parseInt(musicArrayList.get(i).getId_music()));
            }
        }
        datasource.close();
    }

    private void setListeners() {
    }

    private void setupViewPager(ViewPager viewPager) {
        Bundle bundle = new Bundle();
        TimeFragment timeFragment = new TimeFragment();
        DateFragment dateFragment = new DateFragment();
        OptionsFragment optionsFragment = new OptionsFragment();
        timeFragment.setArguments(bundle);
        dateFragment.setArguments(bundle);
        optionsFragment.setArguments(bundle);
        CreateAlarmAdapter adapter = new CreateAlarmAdapter(getSupportFragmentManager());
        adapter.addFragment(timeFragment, "TIME");
        adapter.addFragment(dateFragment, "DATE");
        adapter.addFragment(optionsFragment, "OPTIONS");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onDataPass(String hours, String minutes) {
        alarm.setHours(hours);
        alarm.setMinutes(minutes);
    }

    @Override
    public void onDataPassDay(ToggleButton tbDay, int day) {
        //MONDAY
        if (tbDay.isChecked() && day == 2){
            alarm.setIsMon("true");
        }else if (!tbDay.isChecked() && day == 2){
            alarm.setIsMon("false");
        }
        //TUESDAY
        if (tbDay.isChecked() && day == 3){
            alarm.setIsTue("true");
        }else if (!tbDay.isChecked() && day == 3){
            alarm.setIsTue("false");
        }
        //WEDNESDAY
        if (tbDay.isChecked() && day == 4){
            alarm.setIsWed("true");
        }else if (!tbDay.isChecked() && day == 4){
            alarm.setIsWed("false");
        }
        //THURSDAY
        if (tbDay.isChecked() && day == 5){
            alarm.setIsThu("true");
        }else if (!tbDay.isChecked() && day == 5){
            alarm.setIsThu("false");
        }
        //FRIDAY
        if (tbDay.isChecked() && day == 6){
            alarm.setIsFri("true");
        }else if (!tbDay.isChecked() && day == 6){
            alarm.setIsFri("false");
        }
        //SATURDAY
        if (tbDay.isChecked() && day == 7){
            alarm.setIsSat("true");
        }else if (!tbDay.isChecked() && day == 7){
            alarm.setIsSat("false");
        }
        //SUNDAY
        if (tbDay.isChecked() && day == 1){
            alarm.setIsSun("true");
        }else if (!tbDay.isChecked() && day == 1){
            alarm.setIsSun("false");
        }
    }

    @Override
    public void onDataPassOptionsChallenge(int challenge) {
        alarm.setChallenge(""+challenge);
    }

    @Override
    public void onDataPassOptionsMusic(ArrayList selectedItems) {
        datasource.open();
        musicArrayList = datasource.getMusicWithName(selectedItems);
        datasource.close();
    }

    @Override
    public void onDataPassOptionsSmartRise(boolean smartRise) {
        alarm.setSmartRise(""+smartRise);
    }

    @Override
    public void onDataPassOptionsVibration(int vibration) {
        alarm.setVibration(""+vibration);
    }

    @Override
    public void onDataPassDate(DatePicker datePicker) {
        alarm.setYear(""+datePicker.getYear());
        alarm.setMonth(""+datePicker.getMonth());
        alarm.setDay(""+datePicker.getDayOfMonth());
    }

    @Override
    public void onDataPassWhich(int which) {
        alarm.setIsDayOrDate(""+which);
    }
}
